//
//  AllParentsTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class AllParentsTableViewCell: UITableViewCell {
  
     var deleteBtn: (() -> Void)? = nil

    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var schoolNameTf: UITextField!
    @IBOutlet weak var branchTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var childTblVwHeight: NSLayoutConstraint!
    @IBOutlet weak var childTableView: UITableView!

     var childs: Array<UserModel> =  Array<UserModel>()
    
    

    var tableViewHeight: CGFloat {
        childTableView.layoutIfNeeded()
        
        return childTableView.contentSize.height
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.childTableView.delegate = self
        self.childTableView.dataSource = self
       // childTblVwHeight.constant = tableViewHeight

childTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
       
        // Initialization code
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey]
            {
                let newsize  = newvalue as! CGSize
                childTblVwHeight.constant = newsize.height
            }
        }
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteBtnTap(_ sender: Any) {
        deleteBtn!()
    }
   
    

}

extension AllParentsTableViewCell:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ChildsTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ChildsTableViewCell)!
        let child = childs[indexPath.row]
        cell.nameTf.text = child.userName
        cell.genderTF.text = child.gender

        cell.childImageView.setImage(url: URL(string: child.profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
        return cell
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
