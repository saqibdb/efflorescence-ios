//
//  AllSchoolsTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class AllSchoolsTableViewCell: UITableViewCell {
     var deleteBtn: (() -> Void)? = nil
    var editBtn: (() -> Void)? = nil

    @IBOutlet weak var schoolAdress: UITextField!
    @IBOutlet weak var schoolName: UITextField!
    @IBOutlet weak var viewTopConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    @IBAction func deleteBtnTap(_ sender: Any) {
        deleteBtn!()
    }
    
    @IBAction func editBtnTap(_ sender: Any) {
        editBtn!()
    }
}
