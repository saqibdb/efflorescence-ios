//
//  StudentCollectionViewCell.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 5/13/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import CheckmarkCollectionViewCell

class StudentCollectionViewCell: CheckmarkCollectionViewCell {
    
    @IBOutlet weak var studentProfilePictureView: UIImageView!
    
    @IBOutlet weak var studentName: UILabel!
    
    @IBOutlet weak var selectAllView: UIView!
    
    
    
    
}
