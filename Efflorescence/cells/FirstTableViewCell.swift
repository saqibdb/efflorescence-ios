//
//  FirstTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 26/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import DropDown

class FirstTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subjectName: UITextField!
    var subjectResponse: AllSubjectsResponseData!
    var totalSubjects = [Subjectss]()
    var subjectInitialValue = 1
    let subjectDropDown = DropDown()
    @IBOutlet weak var subjectDropBtn: UIButton!
     var actionBlock: (() -> Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        allSubjectsAPI(page: subjectInitialValue)
        subjectDropDown.anchorView = subjectDropBtn
        subjectDropDown.dataSource = []
        subjectDropDown.width = 200
        subjectDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.subjectDropDown.hide()
            self.subjectName.text = item
            
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func allSubjectsAPI(page: Int)  {
        //  SwiftSpinner.show("Fetching...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth, "page" : page, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSubjects)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        
        Alamofire.request(request).responseJSON { response in
            
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSubjectsResponseData>().map(JSONString: json){
                        self.subjectResponse = dataObject
                        
                        
                        for i in 0..<self.subjectResponse!.subjects!.count {
                            self.totalSubjects.append(self.subjectResponse!.subjects![i])
                        }
                        
                        if self.subjectResponse.nextPage == true {
                            self.subjectInitialValue = self.subjectInitialValue + 1
                            self.allSubjectsAPI(page: self.subjectInitialValue)
                            
                        }
                        else {
                            for i in 0..<self.totalSubjects.count{
                                self.subjectDropDown.dataSource.append(self.totalSubjects[i].subjectName!)
                            }
                            
                         
                            
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    
    @IBAction func subjectDropBtnTap(_ sender: Any) {
        actionBlock?()
        subjectDropDown.show()
        
    }
}
