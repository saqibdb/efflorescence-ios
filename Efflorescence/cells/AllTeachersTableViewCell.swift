//
//  AllTeachersTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class AllTeachersTableViewCell: UITableViewCell {
    var deleteBtn: (() -> Void)? = nil
    var editBtn: (() -> Void)? = nil

    
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    
    
    @IBOutlet weak var lastLoginTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var schoolNameTF: UITextField!
    @IBOutlet weak var branchNameTF: UITextField!
    @IBOutlet weak var classNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var teacherName: UITextField!
    @IBOutlet weak var teacherImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        teacherImageView.layer.cornerRadius = teacherImageView.frame.height / 2
        teacherImageView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteBtnTap(_ sender: Any) {
        deleteBtn!()
    }
    @IBAction func editBtnTap(_ sender: Any) {
        editBtn!()
    }
}
