//
//  CommentTableViewCell.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/25/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var commentCreatorImage: UIImageView!
    @IBOutlet weak var creatorName: UILabel!
    @IBOutlet weak var commentText: UILabel!
    
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
