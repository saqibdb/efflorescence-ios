//
//  AddNewParentTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 28/06/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import GRView
import DropDown

class AddNewParentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var selectProfilePicBtn: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var childName: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var picImageView: UIImageView!
    @IBOutlet weak var picView: GRView!
    
    
    @IBOutlet weak var assignedClassTF: UITextField!
    
    @IBOutlet weak var assignedClassButton: UIButton!
    
    
    
    var imagePickerBtn : (() -> Void)? = nil
    var removePic : (() -> Void)? = nil
    var genderBtn: (() -> Void)? = nil
    let genderDropdown = DropDown()
    
    var assignedClassBtn: (() -> Void)? = nil
    let assignedClassDropdown = DropDown()
    var childNameChangedAct: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        genderDropdown.anchorView = genderButton
        genderDropdown.dataSource = ["Male", "Female"]
        
        
        assignedClassDropdown.anchorView = assignedClassButton
        assignedClassDropdown.dataSource = []
        
        
        
        
        
        picView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func genderBtnTap(_ sender: Any) {
        genderBtn?()
    }
    
    @IBAction func removePicBtnTap(_ sender: Any) {
        removePic?()
        
    }
    @IBAction func selectProfilePicTap(_ sender: Any) {
        imagePickerBtn?()
        
    }
    
    @IBAction func assignedClass(_ sender: Any) {
        assignedClassBtn?()
    }
    
    
    

    
    
    
    @IBAction func childNameChanged(_ sender: UITextField) {
        childNameChangedAct?()
    }
    
    
    
}
