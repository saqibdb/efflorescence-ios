//
//  ChildsTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class ChildsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var childImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        childImageView.layer.cornerRadius = childImageView.frame.height / 2
        childImageView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
