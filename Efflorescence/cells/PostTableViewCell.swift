//
//  PostTableViewCell.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/22/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDate: UILabel!
    
    @IBOutlet weak var postDescription: UILabel!
    
    @IBOutlet weak var subjectsText: UILabel!
    
    @IBOutlet weak var postLikeBtn: UIButton!
    
    @IBOutlet weak var postCommentBtn: UIButton!
    
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
