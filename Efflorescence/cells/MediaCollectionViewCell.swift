//
//  MediaCollectionViewCell.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/29/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class MediaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var removeImage: UIButton!
    
    
}
