//
//  SecondTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 26/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class SecondTableViewCell: UITableViewCell {

    @IBOutlet weak var addSubjectBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
