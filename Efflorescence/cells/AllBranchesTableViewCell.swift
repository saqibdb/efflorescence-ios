//
//  AllBranchesTableViewCell.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit

class AllBranchesTableViewCell: UITableViewCell {
    var deleteBtn: (() -> Void)? = nil
    var editBtn: (() -> Void)? = nil
    @IBOutlet weak var branchAddressTF: UITextField!
    @IBOutlet weak var branchNameTF: UITextField!
    @IBOutlet weak var schoolNameTF: UITextField!
    
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteBtnTap(_ sender: Any) {
        deleteBtn!()
    }
    @IBAction func editBtnTap(_ sender: Any) {
        editBtn!()
    }
    
}
