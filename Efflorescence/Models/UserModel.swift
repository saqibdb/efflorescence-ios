//
//  UserModel.swift
//  uChoose
//
//  Created by iBuildX on 11/07/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class UserModel: HandyJSON {
    var id: Int!
    var userName: String!
    var email: String!
    var type: String!
    var age: String!
    var phone: String!
    var gender: String!
    var profilePictureURL: String!
    var deviceToken: String!
    var authToken: String!
    var isNotification: Bool!
    var isPasswordPinChangeNeeded: Bool!
    var lastLoginDate : Date!
    var school: SchoolModel!
    var branch: BranchModel!
    var fullName: String!

    var classId : Int!
    var localImage : UIImage!
    var childs: Array<UserModel>?

    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            lastLoginDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        
    }
    func didFinishMapping() {
        if self.type == "S-Admin"{
            self.type = "SchoolAdmin"
        }
        if self.type == "B-Admin"{
            self.type = "BranchAdmin"
        }
    }
    
    required init() {}
}


/*
 
 "id": 5,
 "userName": "testiOS",
 "email": "testios@ibuildx.com",
 "type": "SuperAdmin",
 "age": "29",
 "phone": "03465705958",
 "gender": "Male",
 "profilePictureURL": "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiS09LIrtfhAhWC_qQKHZPvBO0QjRx6BAgBEAU&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FMona_Lisa&psig=AOvVaw3akWb6YNiYHBa5lxsLe-r",
 "deviceToken": "",
 "lastLoginDate": "2019-04-17T14:43:59.125313Z",
 "authToken": "c62a16a0-2299-49be-bfe9-570353e05e78",
 "isPasswordPinChangeNeeded": true
 
 */
