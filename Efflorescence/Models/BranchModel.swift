//
//  BranchModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/26/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class BranchModel: HandyJSON {
    var id: Int!
    var updationDate: Date!
    var branchId: Int!
    var branchName: String!
    var teachers: Array<UserModel>?
    var school: SchoolModel!
    var creationDate: Date!
    var schoolId: Int!

    func mapping(mapper: HelpingMapper) {
        mapper <<<
            updationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    required init() {}
}
/*

{
    "school": {
        "updationDate": "2019-03-02T13:19:32.526375Z",
        "schoolAddress": "school address 1",
        "creationDate": "2019-03-02T13:19:32.526270Z",
        "id": 1,
        "schoolName": "school 1"
    },
    "schoolId": 1,
    "branchAddress": "branch address 1",
    "branchName": "branch 1",
    "updationDate": "2019-03-02T13:20:11.568054Z",
    "creationDate": "2019-03-02T13:20:11.567977Z",
    "id": 1
},
*/
