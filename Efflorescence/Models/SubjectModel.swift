//
//  SubjectModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 5/14/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class SubjectModel: HandyJSON {
    var id: Int!
    var subjectName: String!
    
    required init() {}

}
