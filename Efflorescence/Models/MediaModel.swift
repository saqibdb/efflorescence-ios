//
//  MediaModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/22/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class MediaModel: HandyJSON {
    var id: Int!
    var mediaType: String!
    var mediaLink: String!
    var creationDate: Date!
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    
    required init() {}
}


/*
 
 {
 "mediaType": "image",
 "creationDate": "2019-03-02T14:47:31.867393Z",
 "id": 3,
 "mediaLink": "www.abc.com"
 }
 
 */
