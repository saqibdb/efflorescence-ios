//
//  PostModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/22/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class PostModel: HandyJSON {
    var id: Int!
    var classId: Int!
    var liking_users: Array<UserModel>?
    var postDescription: String!
    var total_comments: Int!
    var creator: UserModel!
    var tagged_users: Array<UserModel>?
    var comments: Array<CommentModel>?
    var subjects: Array<SubjectModel>?


    var postTitle: String!
    var updationDate: Date!
    var medias: Array<MediaModel>?
    var total_likes: Int!
    var creatorId: Int!
    var creationDate: Date!

    func mapping(mapper: HelpingMapper) {
        mapper <<<
            updationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    
    required init() {}
}


/*

{
    "Post": {
        "classId": 0,
        "liking_users": [],
        "postDescription": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ",
        "total_comments": 2,
        "creator": {
            "userName": "testiOS",
            "gender": "Male",
            "age": "29",
            "email": "testios@ibuildx.com",
            "phone": "3465705958",
            "type": "SuperAdmin",
            "id": 7,
            "profilePictureURL": "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiS09LIrtfhAhWC_qQKHZPvBO0QjRx6BAgBEAU&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FMona_Lisa&psig=AOvVaw3akWb6YNiYHBa5lxsLe-r"
        },
        "tagged_users": [
        {
        "userName": "test-superAdmin1",
        "gender": "Male",
        "age": "29",
        "email": "testSuperAdmin1@ibuildx.com",
        "phone": "3465705958",
        "lastLoginDate": "2019-03-02T13:17:18.530771Z",
        "type": "SuperAdmin",
        "id": 1,
        "profilePictureURL": "www.abc.com"
        },
        {
        "userName": "test-teacher1",
        "gender": "Female",
        "age": "29",
        "email": "test-teacher1@ibuildx.com",
        "phone": "923465705958",
        "lastLoginDate": "2019-03-02T13:34:34.917327Z",
        "type": "Teacher",
        "id": 4,
        "profilePictureURL": "www.xyz.com"
        }
        ],
        "comments": [
        {
        "commentMessage": "THIS IS THE SECOND COMMENT",
        "creator": {
        "userName": "test-superAdmin1",
        "gender": "Male",
        "age": "29",
        "email": "testSuperAdmin1@ibuildx.com",
        "phone": "3465705958",
        "lastLoginDate": "2019-03-02T13:17:18.530771Z",
        "type": "SuperAdmin",
        "id": 1,
        "profilePictureURL": "www.abc.com"
        },
        "userId": 1,
        "updationDate": "2019-04-26T12:16:18.079022Z",
        "creationDate": "2019-04-26T12:16:18.078924Z",
        "id": 2,
        "commentTitle": "2nd Comment"
        },
        {
        "commentMessage": "THIS IS THE FIRST COMMENT",
        "creator": {
        "userName": "test-superAdmin1",
        "gender": "Male",
        "age": "29",
        "email": "testSuperAdmin1@ibuildx.com",
        "phone": "3465705958",
        "lastLoginDate": "2019-03-02T13:17:18.530771Z",
        "type": "SuperAdmin",
        "id": 1,
        "profilePictureURL": "www.abc.com"
        },
        "userId": 1,
        "updationDate": "2019-04-26T12:07:16.625816Z",
        "creationDate": "2019-04-26T12:07:16.625642Z",
        "id": 1,
        "commentTitle": "1st Comment"
        }
        ],
        "postTitle": "TEST post 2",
        "updationDate": "2019-04-25T13:15:36.444585Z",
        "medias": [
        {
        "mediaType": "image",
        "creationDate": "2019-04-25T13:15:36.452182Z",
        "id": 10,
        "mediaLink": "https://cache-graphicslib.viator.com/graphicslib/page-images/742x525/271052_Viator_Shutterstock_166828.jpg"
        },
        {
        "mediaType": "video",
        "creationDate": "2019-04-25T13:15:36.467053Z",
        "id": 11,
        "mediaLink": "https://images.musement.com/cover/0001/91/thumb_90152_cover_header.jpeg"
        }
        ],
        "total_likes": 0,
        "creatorId": 7,
        "creationDate": "2019-04-25T13:15:36.444508Z",
        "id": 7
    }
}

*/
