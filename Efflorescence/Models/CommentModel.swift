//
//  CommentModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/26/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class CommentModel: HandyJSON {
    var id: Int!
    var commentTitle: String!
    var commentMessage: String!
    var userId: Int!
    var creationDate: Date!
    var updationDate: Date!
    var creator: UserModel!
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            updationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    required init() {}
}
/*
{
    "commentMessage": "THIS IS THE SECOND COMMENT",
    "creator": {
        "userName": "test-superAdmin1",
        "gender": "Male",
        "age": "29",
        "email": "testSuperAdmin1@ibuildx.com",
        "phone": "3465705958",
        "lastLoginDate": "2019-03-02T13:17:18.530771Z",
        "type": "SuperAdmin",
        "id": 1,
        "profilePictureURL": "www.abc.com"
    },
    "userId": 1,
    "updationDate": "2019-04-26T12:16:18.079022Z",
    "creationDate": "2019-04-26T12:16:18.078924Z",
    "id": 2,
    "commentTitle": "2nd Comment"
},
*/
