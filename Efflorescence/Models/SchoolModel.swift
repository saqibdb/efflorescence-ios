//
//  SchoolModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/26/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class SchoolModel: HandyJSON {
    var id: Int!
    var updationDate: Date!
    var branchId: Int!
    var schoolName: String!
    var teachers: Array<UserModel>?
    var creationDate: Date!
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            updationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    required init() {}
}
