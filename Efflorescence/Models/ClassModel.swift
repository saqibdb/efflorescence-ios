//
//  ClassModel.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/26/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class ClassModel: HandyJSON {
    var id: Int!
    var updationDate: Date!
    var branchId: Int!
    var className: String!
    var teachers: Array<UserModel>?
    var creationDate: Date!
    var branch: BranchModel!
    var subjects: Array<SubjectModel>?


    func mapping(mapper: HelpingMapper) {
        mapper <<<
            updationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        mapper <<<
            creationDate <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
    }
    required init() {}
}
/*
{
    "updationDate": "2019-03-02T13:27:45.679965Z",
    "branchId": 1,
    "className": "Geography",
    "teachers": [
    {
    "userName": "test-teacher1",
    "studentIdentification": "",
    "gender": "Female",
    "age": "29",
    "email": "test-teacher1@ibuildx.com",
    "updationDate": "2019-03-02T13:34:34.917291Z",
    "phone": "923465705958",
    "isNotification": false,
    "lastLoginDate": "2019-03-02T13:34:34.917327Z",
    "creationDate": "2019-03-02T13:34:34.917217Z",
    "type": "Teacher",
    "id": 4,
    "deviceToken": "12345",
    "profilePictureURL": "www.xyz.com"
    }
    ],
    "branch": {
        "school": {
            "updationDate": "2019-03-02T13:19:32.526375Z",
            "schoolAddress": "school address 1",
            "creationDate": "2019-03-02T13:19:32.526270Z",
            "id": 1,
            "schoolName": "school 1"
        },
        "schoolId": 1,
        "branchAddress": "branch address 1",
        "branchName": "branch 1",
        "updationDate": "2019-03-02T13:20:11.568054Z",
        "creationDate": "2019-03-02T13:20:11.567977Z",
        "id": 1
    },
    "creationDate": "2019-03-02T13:27:45.679762Z",
    "id": 1
},
*/
