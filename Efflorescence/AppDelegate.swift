//
//  AppDelegate.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/16/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import CoreData
import FAPanels
import UserNotifications
import SwiftSpinner
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         //Override point for customization after application launch.
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rightMenuVC: MenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let centerNavVC = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigation") as! UINavigationController
        
        
        
        //  Set the Panel controllers with just two lines of code
        
        let rootController: FAPanelController = window?.rootViewController as! FAPanelController
        rootController.rightPanelPosition = .front
        rootController.leftPanelPosition = .front

        _ = rootController.center(centerNavVC).left(rightMenuVC).right(nil)
        UNUserNotificationCenter.current().delegate = self
        UIApplication.shared.applicationIconBadgeNumber = 0
        MSAppCenter.start("c244b3e3-3b79-48d3-be5e-9220cd503133", withServices:[
          MSAnalytics.self,
          MSCrashes.self
        ])

        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("TOKEN IS \(token)")
        //send this device token to server
        
        if let userObj : UserModel = Utility.getUserFromPreference(){
            let parameters : [String : Any] = ["registration_id" : token ,
                                               "device_id" : UIDevice.current.identifierForVendor!.uuidString,
                                               "auth_token":userObj.authToken]
            
            
            Utility.registerPushApns(withParameters: parameters) { (status, error, response) in
                if status == false {
                    print("ERROR registerPushApns= \(error!)")
                }
                else{
                    print("RESPONSE registerPushApns= \(response!)")
                }
            }
        }
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Efflorescence")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// MARK: - Extension

extension UIApplication {
    
}

// MARK: - Notifications Delegate
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    
    
    
    //Called if unable to register for APNS.
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
        print(error)
        
    }
    
    
    // This method will be called when app received push notifications in foreground
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Recived Push")
        /*_ = (userInfo["aps"] as! NSDictionary)["alert"] as! NSDictionary
         _ = userInfo["sendbird"] as! NSDictionary
         */
        // Your custom way to parse data
        //let topVC = UIApplication.topViewController()
        if let aps = userInfo["aps"] as? NSDictionary {
            var alertMsg = ""
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["message"] as? NSString {
                    alertMsg = message as String
                }
            } else if let alert = aps["alert"] as? NSString {
                alertMsg = alert as String
            }
            
            AlertsPopUps.showNotificationMessage(title: "Notification", desc: alertMsg, textColor: .white)
        }
        
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Recived Push")
        let content = response.notification.request.content.userInfo
        if let postId = content["postId"] as? Int {
            
            print("Recived Push \(postId)")


            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            destinationViewController.postID = postId
            
            let topVC = UIApplication.topViewController() as! FAPanelController
            
            let mainNavigationController = topVC.center as! UINavigationController
            
            
            if mainNavigationController.visibleViewController is PostDetailViewController {
                print("GOOD CONTROLLER")
                let postDetailVC = mainNavigationController.visibleViewController as! PostDetailViewController
                if postDetailVC.selectedPost != nil {
                    if postDetailVC.selectedPost.id == postId {
                        SwiftSpinner.show("Refreshing...")
                        postDetailVC.getDetailsOfPost()
                        return
                    }
                }
                if postDetailVC.postID == postId {
                    SwiftSpinner.show("Refreshing...")
                    postDetailVC.getDetailsOfPost()
                    return
                }
                
            }
            mainNavigationController.pushViewController(destinationViewController, animated: true)

            
            
            
            
            /*
            
            
            
            let navigationController = self.window?.rootViewController as! UINavigationController
            
            navigationController.pushViewController(destinationViewController, animated: true)
            */
            





        }
        
        completionHandler()
    }
    
}
