//
//  PinLoginViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/18/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SVPinView
import SVPinView
import SwiftSpinner
import SwiftEntryKit
import HandyJSON


class PinLoginViewController: UIViewController {
    @IBOutlet weak var pinView: SVPinView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

    }
    
    
    
    @IBAction func nextClicked(_ sender: UIButton){
        
        let pin = self.pinView.getPin()
        
        //let pin = "000000"
        
        if pin.count < 6 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill Pin Completely.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        SwiftSpinner.show("Validating...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let parameters : [String : Any] = ["user_name" : userObj.userName! ,
                                           "pin" : pin]
        
        print(parameters)
        Utility.loginWithPin(withParameters: parameters) { (status, error, response) in
            if status == false {
                print("ERROR = \(error!)")
                SwiftSpinner.hide()
                AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let userDict = (response!["User"] as? [String : Any]){
                    if let userObj = UserModel.deserialize(from: userDict) {
                        print("User Name = \(userObj.userName!)")
                        Utility.saveUserInPreference(User: userObj)
                        self.performSegue(withIdentifier: "ToTimeline", sender: self)
                        SwiftSpinner.hide()
                    }
                }
            }
        }
        
        
        
        
        
        
    }

    
    @IBAction func loginWithDifferent(_ sender: UIButton) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"ffflorescenceLoggedUser")
        prefs.synchronize()
        self.navigationController?.popToRootViewController(animated: true)

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
