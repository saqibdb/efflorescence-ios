//
//  AllTeachersViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftSpinner

class AllTeachersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var allTeachersTV: UITableView!
    
    var refreshControl = UIRefreshControl()

    
    var cellCount = 5
    var allTeachersResponse: AllTeachersResponseModel!
    var totalResponse = [TeachersData]()
    var initialValue = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allTeachersTV.delegate = self
        allTeachersTV.dataSource = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.allTeachersTV.addSubview(refreshControl) // not required when using UITableViewController
       
    }
    override func viewDidAppear(_ animated: Bool) {
        allTeachersAPI(page: initialValue)
    }
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        initialValue = 1
        SwiftSpinner.show("Fetching...")
        allTeachersAPI(page: initialValue)

    }
    
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func addTeacherBtnTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewTeacherViewController") as? AddNewTeacherViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTeachersResponse != nil {
            return totalResponse.count
        }
        else{
             return 0
        }
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allTeachersTV.dequeueReusableCell(withIdentifier: "cell") as! AllTeachersTableViewCell
        cell.teacherImageView.setImage(url: URL(string: totalResponse[indexPath.row].profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
        cell.teacherName.text = totalResponse[indexPath.row].fullName!
        cell.emailTF.text = totalResponse[indexPath.row].email
        cell.phoneNoTF.text = totalResponse[indexPath.row].phone
        cell.genderTF.text = totalResponse[indexPath.row].gender
        if totalResponse[indexPath.row].classes?.count != 0 {
            cell.classNameTF.text = totalResponse[indexPath.row].classes?[0].className
        }
       
        cell.branchNameTF.text = totalResponse[indexPath.row].branch?.branchName
        cell.schoolNameTF.text = totalResponse[indexPath.row].branch?.school?.schoolName
        let string = totalResponse[indexPath.row].lastLoginDate
        if let range = string!.range(of: "T") {
            let firstPart = string![string!.startIndex..<range.lowerBound]
            print(firstPart) // print Hello
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let showDate = inputFormatter.date(from: String(firstPart))
            inputFormatter.dateFormat = "dd MMM yyyy"
            let resultString = inputFormatter.string(from: showDate!)
            print(resultString)
            cell.lastLoginTF.text = resultString
            if totalResponse[indexPath.row].classes?.count != 0 {
                cell.classNameTF.text = totalResponse[indexPath.row].classes?.first?.className
            }
        }
        cell.deleteBtn = {
            self.removeTeachersAPI(teacher_id: self.totalResponse[indexPath.row].id!)
            self.totalResponse.remove(at: indexPath.row)
            self.allTeachersTV.reloadData()
        }
        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewTeacherViewController") as? AddNewTeacherViewController
                vc?.selectedTeacherData = self.totalResponse[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
        return cell
    }
    func allTeachersAPI(page: Int)  {
        if totalResponse.count == 0 {
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : page, "per_page" : 1000]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allTeachers)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllTeachersResponseModel>().map(JSONString: json){
                        self.totalResponse.removeAll()
                        self.allTeachersResponse = dataObject
                        
                        for i in 0..<self.allTeachersResponse!.teachers!.count {
                            self.totalResponse.append(self.allTeachersResponse!.teachers![i])
                        }
                        if self.allTeachersResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allTeachersAPI(page: self.initialValue)
                            
                        }
                        else {
                            
                            self.allTeachersTV.reloadData()
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    func removeTeachersAPI(teacher_id: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "teacher_id" : teacher_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeTeacher)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllTeachersResponseModel>().map(JSONString: json){
                     self.initialValue = 1
                     self.allTeachersAPI(page: self.initialValue)
                     print("removed") 
                    }
                }
            }
        }
    }
    
  

}
