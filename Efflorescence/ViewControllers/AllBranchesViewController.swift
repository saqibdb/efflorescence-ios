//
//  AllBranchesViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import ObjectMapper
import Alamofire

class AllBranchesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var allBranchesTV: UITableView!
    
    @IBOutlet weak var addNewBranchBtn: UIButton!
 
    
    var refreshControl = UIRefreshControl()

    
    var branchesResponse: AllBranchesResponse!
    var totalResponse = [Branches]()
    var initialValue = 1


    override func viewDidLoad() {
        super.viewDidLoad()
        allBranchesTV.delegate = self
        allBranchesTV.dataSource = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.allBranchesTV.addSubview(refreshControl) // not required when using UITableViewController
        
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        if userObj.type == "BranchAdmin"{
            self.addNewBranchBtn.isHidden = true
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        allBranchAPI(page: initialValue)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        initialValue = 1
        SwiftSpinner.show("Fetching...")
        allBranchAPI(page: initialValue)

    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if branchesResponse != nil {
            return totalResponse.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allBranchesTV.dequeueReusableCell(withIdentifier: "cell") as! AllBranchesTableViewCell
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        if userObj.type == "BranchAdmin"{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        
        
        
        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewBranchViewController") as? AddNewBranchViewController
            vc?.edit = true
            vc?.branchName = self.totalResponse[indexPath.row].branchName
            vc?.branchAddress = self.totalResponse[indexPath.row].branchAddress
            vc?.schoolName = self.totalResponse[indexPath.row].school?.schoolName
            vc?.selectedBranch = self.totalResponse[indexPath.row]
            
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        cell.deleteBtn = {
          
            let alert = UIAlertController(title: "Alert!", message: "Do you want to remove this branch", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
                
                
                  self.deleteBranch(branch_id: self.totalResponse[indexPath.row].id!)
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
        cell.schoolNameTF.text = totalResponse[indexPath.row].school?.schoolName
        cell.branchNameTF.text = totalResponse[indexPath.row].branchName
        cell.branchAddressTF.text = totalResponse[indexPath.row].branchAddress
        
    
        
        return cell
    }
    @IBAction func addBranchTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewBranchViewController") as? AddNewBranchViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func deleteBranch(branch_id: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "branch_id" : branch_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeBranches)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.status == "Branch has been removed." {
                            self.initialValue = 1
                            self.allBranchAPI(page: self.initialValue)
                            print("removed")
                        }
                    }
                }
            }
        }
    }

    func allBranchAPI(page: Int)  {
        if totalResponse.count == 0 {
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : -1, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allBranches)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
           SwiftSpinner.hide()

            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllBranchesResponse>().map(JSONString: json){
                        self.totalResponse.removeAll()
                        self.branchesResponse = dataObject
                       
                       
                            for i in 0..<self.branchesResponse!.branches!.count {
                                self.totalResponse.append(self.branchesResponse!.branches![i])
                            }
                        
                         if self.branchesResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allBranchAPI(page: self.initialValue)
                            self.allBranchesTV.reloadData()
                            
                        }
                        else {
                            
                            SwiftSpinner.hide()
                             self.allBranchesTV.reloadData()
                        }
                    }
                }
            }
        }
    }
}
