//
//  AddNewSubjectViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 10/8/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftSpinner

class AddNewSubjectViewController: UIViewController {
    
    @IBOutlet weak var schoolNameTF: UITextField!
    
    @IBOutlet weak var headerLbl: UILabel!
    
    var selectedSubject : SubjectModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if selectedSubject != nil {
            headerLbl.text = "Edit Subject"
            schoolNameTF.text = selectedSubject.subjectName
        }
        
        
    }
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAndAddAnotherTap(_ sender: Any) {
        
        if schoolNameTF.text == "" {
            let alert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else {
            if selectedSubject != nil {
                self.editSubject()
            }
            else {
                self.addNewSubject()
            }
        }
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    
    
    
    func addNewSubject() {
       if self.schoolNameTF.text?.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
            "subject_name" : schoolNameTF.text!]
        Utility.addNewSubject(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let status = (response!["Status"] as? String){
                    if status == "New subject added"{
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is AllAdminsViewController {
                            let topVC = UIApplication.topViewController() as! AllSubjectsViewController
                            topVC.getSubjects()
                        }
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Adding... \(Int(progress!*100.0))%", animated: true)
        })
    }
 
    func editSubject() {
        if self.schoolNameTF.text?.count == 0{
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
            "subject_name" : schoolNameTF.text!,
            "subject_id" : self.selectedSubject.id! ]
        Utility.editSubject(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let subjectDict = (response!["Edited Subject"] as? [String:Any]){                    self.navigationController?.popViewController(animated: true)
                    if UIApplication.topViewController() is AllAdminsViewController {
                        let topVC = UIApplication.topViewController() as! AllSubjectsViewController
                        topVC.getSubjects()
                    }
                }
                else{
                    AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: (response?.description)!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                }
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Adding... \(Int(progress!*100.0))%", animated: true)
        })
    }
}
