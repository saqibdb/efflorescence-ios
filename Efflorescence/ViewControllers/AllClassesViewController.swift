//
//  AllClassesViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import ObjectMapper

class AllClassesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var allClassesTV: UITableView!
    var refreshControl = UIRefreshControl()

    var classesResponse: AllClassesResponse!
    var totalResponse = [Classes]()
    
   
    var initialValue = 1


    override func viewDidLoad() {
        super.viewDidLoad()
        allClassesTV.delegate = self
        allClassesTV.dataSource = self
       
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.allClassesTV.addSubview(refreshControl) // not required when using UITableViewController
       
    }
    override func viewDidAppear(_ animated: Bool) {
         allClassesAPI(page: initialValue)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        initialValue = 1
        SwiftSpinner.show("Fetching...")
        allClassesAPI(page: initialValue)

    }
    
    
    
    
    
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if classesResponse != nil {
            return totalResponse.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allClassesTV.dequeueReusableCell(withIdentifier: "cell") as! AllClassesTableViewCell
        cell.deleteBtn = {
            let alert = UIAlertController(title: "Alert!", message: "Do you want to remove this School", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
                
                
                self.deleteClass(class_id: (self.totalResponse[indexPath.row].id)!)
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewClassViewController") as? AddNewClassViewController
            vc?.edit = 1
            vc?.classPassResponse = self.totalResponse[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        cell.schoolNameTF.text = totalResponse[indexPath.row].branch?.school?.schoolName
        cell.branchNameTF.text = totalResponse[indexPath.row].branch?.branchName
        cell.classNameTF.text = totalResponse[indexPath.row].className
        if totalResponse[indexPath.row].teachers?.count == 0 {
            cell.assignedTeacherTF.text = "No assigned teacher"
        }
        else {
             cell.assignedTeacherTF.text = totalResponse[indexPath.row].teachers?.first?.userName
        }
        cell.subjectsTagView.removeAllTags()

        if totalResponse[indexPath.row].subjects!.count > 0 {
            for subject in totalResponse[indexPath.row].subjects!{
                cell.subjectsTagView.addTag(subject.subjectName!)
            }
        }
        
        return cell
    }
    
    @IBAction func addClassBtnTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewClassViewController") as? AddNewClassViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func allClassesAPI(page: Int)  {
        if totalResponse.count == 0 {
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : page, "per_page" : 1000]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allClasses)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
           SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllClassesResponse>().map(JSONString: json){
                        self.totalResponse.removeAll()

                        self.classesResponse = dataObject
                        
                            for i in 0..<self.classesResponse!.classes!.count {
                                self.totalResponse.append(self.classesResponse!.classes![i])
                            }
                        if self.classesResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allClassesAPI(page: self.initialValue)
                           
                        }
                        else {
                            
                            self.allClassesTV.reloadData()
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    func deleteClass(class_id: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "class_id" : class_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeClass)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.status == "Class has been removed." {
                            self.initialValue = 1
                            self.allClassesAPI(page: self.initialValue)
                            print("removed")
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }


    

   

}
