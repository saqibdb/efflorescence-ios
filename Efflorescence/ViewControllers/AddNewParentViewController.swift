//
//  AddNewParentViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 28/06/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import DropDown
import SwiftSpinner

class AddNewParentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var pswrdTF: UITextField!
    @IBOutlet weak var addChildsTblView: UITableView!
    
    
    
    @IBOutlet weak var branchTF: UITextField!
    @IBOutlet weak var schoolTF: UITextField!
    
    @IBOutlet weak var schoolBtn: UIButton!
    @IBOutlet weak var branchBtn: UIButton!
    
    var allSchools : [SchoolModel] = [SchoolModel]()
    var selectedSchool : SchoolModel!
    var selectedSchoolId : Int!
    
    var allBranches : [BranchModel] = [BranchModel]()
    var selectedBranch : BranchModel!
    var selectedBranchId : Int!
    
    
    let schoolDropDown = DropDown()
    let branchDropDown = DropDown()
    
    var allClasses : [ClassModel] = [ClassModel]()

    
    var addedChilds : [UserModel] = [UserModel]()

    
    
    
    var tableCellCount = 1
    var row = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildsTblView.delegate = self
        addChildsTblView.dataSource = self
        
        self.allSchoolsAPI()
        self.allBranchClassesAPI()
        
        branchDropDown.anchorView = branchBtn
        branchDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.branchTF.text = item
            self.selectedBranch = self.allBranches[index]
            self.selectedBranchId = self.selectedBranch.id!
            self.allBranchClassesAPI()
        }
        schoolDropDown.anchorView = schoolBtn
        schoolDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.schoolTF.text = item
            self.selectedSchool = self.allSchools[index]
            self.selectedSchoolId = self.selectedSchool.id!
            self.allSchoolBranchAPI()
            self.allBranchClassesAPI()
        }
        
        let newChild = UserModel()
        addedChilds.append(newChild)
 
    }
    
    func allSchoolsAPI()  {
        SwiftSpinner.show("Getting Branches...", animated: true)
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: [String : Any] = ["auth_token" : auth!,"page":-1 ,"per_page":-1]
        
        
        Utility.getAllSchools(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let schoolsDicts = (response!["Schools"] as? [[String:Any]]) {
                    self.allBranches.removeAll()
                    self.branchDropDown.dataSource.removeAll()
                    for schoolsDict in schoolsDicts {
                        if let schoolObj = SchoolModel.deserialize(from: schoolsDict) {
                            self.allSchools.append(schoolObj)
                            self.schoolDropDown.dataSource.append(schoolObj.schoolName)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                }
            }
        }
    }
    
    
    func allSchoolBranchAPI()  {
        SwiftSpinner.show("Getting Branches...", animated: true)
        if selectedSchoolId == nil {
            return
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: [String : Any] = ["auth_token" : auth!, "school_id" : self.selectedSchoolId]
        
        
        Utility.getBranchesOfSchool(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let schoolDict = (response!["School"] as? [String:Any]) {
                    if let branchesDicts = (schoolDict["branches"] as? [[String:Any]]) {
                        self.allBranches.removeAll()
                        self.branchDropDown.dataSource.removeAll()
                        for branchesDict in branchesDicts {
                            if let branchObj = BranchModel.deserialize(from: branchesDict) {
                                self.allBranches.append(branchObj)
                                self.branchDropDown.dataSource.append(branchObj.branchName)
                            }
                            else{
                                print("ISSUE IN DESERIALIZE")
                            }
                        }
                    }
                }
            }
        }
    }
    
    func allBranchClassesAPI()  {
        SwiftSpinner.show("Getting Classes...", animated: true)
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        var params: [String:Any] = ["auth_token" : auth!, "page" : -1, "per_page" : 100]
        if self.selectedSchoolId != nil {
            params = ["auth_token" : auth!, "page" : -1, "per_page" : 100,"filters": [["filter_name": "school","filter_value": self.selectedSchoolId]]]
        }
        if self.selectedBranchId != nil {
            params = ["auth_token" : auth!, "page" : -1, "per_page" : 100,"filters": [["filter_name": "branch","filter_value": self.selectedBranchId]]]
        }
        
        
        Utility.getBranchClasses(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let classesDicts = (response!["Classes"] as? [[String:Any]]) {
                    self.allClasses.removeAll()
                    for classesDict in classesDicts {
                        if let classObj = ClassModel.deserialize(from: classesDict) {
                            self.allClasses.append(classObj)
                            self.addChildsTblView.reloadData()

                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    @IBAction func addChildBtn(_ sender: Any) {
        let cell = addChildsTblView.cellForRow(at: NSIndexPath(row: tableCellCount - 1, section: 0) as IndexPath) as! AddNewParentTableViewCell
        if cell.childName.text != "" {
            tableCellCount = tableCellCount + 1
            addChildsTblView.reloadData()
            addChildsTblView.scrollToRow(at: NSIndexPath(row: tableCellCount - 1, section: 0) as IndexPath, at: .bottom, animated: true)
            
            let newChild = UserModel()
            addedChilds.append(newChild)
        }
        
       
    }
    @IBAction func backBtnTap(_ sender: Any) {
         _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellCount
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let chosenImage = info[.originalImage] as? UIImage {
            let cell = addChildsTblView.cellForRow(at: NSIndexPath(row: self.row, section: 0) as IndexPath) as! AddNewParentTableViewCell
            
          let resizedImage = Utility.imageWithImage(sourceImage: chosenImage, scaledToWidth: 800)

            let addedChild = addedChilds[self.row]
            addedChild.localImage = resizedImage
            cell.picImageView.image = chosenImage
             cell.picView.isHidden = false
            cell.selectProfilePicBtn.isHidden = true
            dismiss(animated: true, completion: nil)
            
        } else{
            print("Something went wrong")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AddNewParentTableViewCell


        let addedChild = addedChilds[indexPath.row]
        if addedChild.userName != nil {
            cell.childName.text = addedChild.userName
        }
        else{
            cell.childName.text = ""
        }
        
        if addedChild.gender != nil {
            cell.genderTF.text = addedChild.gender
        }
        else{
            cell.genderTF.text = ""
        }
        
        if addedChild.localImage != nil {
            cell.picImageView.image = addedChild.localImage
             cell.picView.isHidden = false
            cell.selectProfilePicBtn.isHidden = true
        }
        else{
            cell.picView.isHidden = true
            cell.selectProfilePicBtn.isHidden = false
        }
        
        
        

        cell.genderBtn = {
            cell.genderDropdown.show()
        }
        cell.assignedClassBtn = {
            cell.assignedClassDropdown.show()
        }
        cell.childNameChangedAct = {
            print("NAME = \(cell.childName.text!)")
            addedChild.userName = cell.childName.text!
        }
        cell.imagePickerBtn = {
            self.row = indexPath.row
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                print("Button capture")
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = UIImagePickerController.SourceType.photoLibrary;
                //imag.mediaTypes = [kUTTypeImage];
                imag.allowsEditing = false
                imag.view.tag = indexPath.row
                self.present(imag, animated: true, completion: nil)
            }
        }
        cell.removePic = {
            addedChild.localImage = nil
            cell.picImageView.image = nil
            cell.picView.isHidden = true
            cell.selectProfilePicBtn.isHidden = false
        }
        cell.assignedClassDropdown.dataSource.removeAll()
        for classe in self.allClasses{
            cell.assignedClassDropdown.dataSource.append(classe.className)
        }
        
        
        cell.assignedClassDropdown.selectionAction = { [unowned cell] (index: Int, item: String) in
            if index < self.allClasses.count {
                cell.assignedClassTF.text = item
                let selectedClass = self.allClasses[index]
                addedChild.classId = selectedClass.id
            }
            
        }
        
        cell.genderDropdown.selectionAction = { [unowned cell] (index: Int, item: String) in
            cell.genderTF.text = item
            addedChild.gender = cell.genderTF.text
        }
        
        return cell
    }
    
    
    
    @IBAction func schoolDropDownTap(_ sender: Any) {
        schoolDropDown.show()
    }
    @IBAction func selectBranchDropDownTap(_ sender: Any) {
        branchDropDown.show()
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
       
        let firstChild = addedChilds[0]
        
        if self.userNameTF.text?.count == 0 || self.phoneNumberTF.text?.count == 0 || self.emailTF.text?.count == 0 || self.ageTF.text?.count == 0 || self.pswrdTF.text?.count == 0 || self.branchTF.text?.count == 0 || firstChild.userName.count == 0 || firstChild.gender.count == 0 || firstChild.classId == nil || firstChild.localImage == nil{
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Fields.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        
        SwiftSpinner.show("Saving...", animated: true)
        let userObj : UserModel! = Utility.getUserFromPreference()
        let auth = userObj.authToken
        
        
        var childsArray : [[String:Any]] = [[String:Any]]()

        for child in self.addedChilds {
            if child.localImage != nil {
                let childParam : [String:Any] = ["name": child.userName,
                "age": "19",
                "gender": child.gender,
                "class_id":child.classId,
                "profile_picture_data": child.localImage.toBase64()!]
                childsArray.append(childParam)
            }
        }
        let parameters : [String : Any] = ["auth_token" : auth! ,
        "parent_user_name" : self.userNameTF.text!,
        "parent_email" : self.emailTF.text!,
        "parent_password":self.pswrdTF.text!,
        "parent_age":"50",
        "parent_phone":self.phoneNumberTF.text!,
        "parent_gender":"Male",
        "parent_full_name":self.ageTF.text!,
        "branch_id":self.selectedBranchId!,
        "parent_childs":childsArray]
        
        Utility.addParentWithChild(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                
                if let teacherDictDict = (response!["NewParent"] as? [String:Any]){
                    if UserModel.deserialize(from: teacherDictDict) != nil {
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is AllParentsViewController {
                            let topVC = UIApplication.topViewController() as! AllParentsViewController
                            topVC.getAllParents()
                        }
                    }
                    else{
                        print("ISSUE IN DESERIALIZE")
                    }
                }
               
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Saving... \(Int(progress!*100.0))%", animated: true)
        })
        
        
        
    }
    
    
}
