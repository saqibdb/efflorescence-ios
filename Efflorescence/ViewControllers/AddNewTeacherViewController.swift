//
//  AddNewTeacherViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 20/06/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import GRView
import ObjectMapper
import Alamofire
import SwiftSpinner
import DropDown
import SwiftEntryKit
import MobileCoreServices
import ImagePicker
import Photos
import YangMingShan


class AddNewTeacherViewController: UIViewController  {
    
    @IBOutlet weak var selectPicBtn: UIButton!
    @IBOutlet weak var classTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var branchTF: UITextField!
    @IBOutlet weak var schoolTF: UITextField!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicView: GRView!
    
    @IBOutlet weak var titleText: UILabel!
    
    
    
    
    
    var schoolsResponse: AllSchoolResponse!
    var initialValue1 = 1
    var initialValue2 = 1
    var initialValue3 = 1
    var totalSchoolResponse = [Schools]()


    var classesResponse: AllClassesResponse!
    var totalClassResponse = [Classes]()
    let classesDropDown = DropDown()
    let schoolDropDown = DropDown()
    let branchDropDown = DropDown()
    let genderDropDown = DropDown()

    var selectedSchool: Schools!
    var selectedClass: Classes!

    var schoolID: Int!
    var branchId: Int!
    var classId: Int!
    var selectedImage: UIImage!
    var teacherSendData = AddTeacherDataModel()
    
    
    var allBranches : [BranchModel] = [BranchModel]()
    var selectedBranch : BranchModel!
    var selectedBranchId : Int!

    var selectedTeacherData : TeachersData!
    
    var isPictureChanged = false

    
    @IBOutlet weak var schoolBtn: UIButton!
    @IBOutlet weak var branchBtn: UIButton!
    @IBOutlet weak var classBtn: UIButton!
    @IBOutlet weak var genderBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicView.isHidden = true
        allSchoolsAPI(page: initialValue1)
        
        classesDropDown.anchorView = classBtn
        classesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.classTF.text = item
            self.selectedClass = self.totalClassResponse[index]
            self.classId = self.selectedClass.id!
            self.classesDropDown.hide()
        }
        branchDropDown.anchorView = branchBtn
        branchDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.branchTF.text = item
            self.selectedBranch = self.allBranches[index]
            self.selectedBranchId = self.selectedBranch.id!
            self.allClassesAPI(page: self.initialValue3)
            self.classesDropDown.hide()
        }
        schoolDropDown.anchorView = schoolBtn
        schoolDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.schoolTF.text = item
            self.selectedSchool = self.totalSchoolResponse[index]
            self.schoolID = self.selectedSchool.id!
            self.allSchoolBranchAPI()
            self.allClassesAPI(page: self.initialValue3)

            self.classesDropDown.hide()
        }
        genderDropDown.anchorView = genderBtn
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.genderTF.text = item
            
            self.genderDropDown.hide()
        }
        genderDropDown.dataSource = ["Male", "Female"]

        if self.selectedTeacherData != nil {
            mapSelectedTeacher()
        }

    }
    
    
    func mapSelectedTeacher(){
        usernameTF.text = self.selectedTeacherData.userName
        emailTF.text = self.selectedTeacherData.email
        passwordTF.isEnabled = false
        ageTF.text = self.selectedTeacherData.fullName
        
        phoneNoTF.text = self.selectedTeacherData.phone
        genderTF.text = self.selectedTeacherData.gender
        
        if self.selectedTeacherData.branch != nil {
            self.branchId = self.selectedTeacherData.branch?.id
            
            if self.selectedTeacherData.branch?.school != nil {
                self.schoolID = self.selectedTeacherData.branch?.school!.id!
            }
            self.allSchoolBranchAPI()
            self.allClassesAPI(page: self.initialValue3)
            

            profilePicView.isHidden = false
            selectPicBtn.isHidden = true
            profilePic.setImage(url: URL(string: self.selectedTeacherData.profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
            self.titleText.text = "Edit Teacher"
        }
        
        
        self.schoolTF.text = self.selectedTeacherData.branch?.school?.schoolName
        self.branchTF.text = self.selectedTeacherData.branch?.branchName
        if (self.selectedTeacherData!.classes?.count)! > 0{
            let classe = self.selectedTeacherData!.classes?.at(0)
            self.classTF.text = classe?.className
            self.classId = classe?.id
        }
        
        
        
    }
    
    
    @IBAction func saveBtnTap(_ sender: Any) {
        if self.selectedTeacherData != nil {
            editOldTeacher()
        }
        else{
            addnewTeacher()
        }
    }
    @IBAction func selectProfilePicTap(_ sender: Any) {
        
        self.showAlertView()
        
    }
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    

    @IBAction func deleteImageBtnTap(_ sender: Any) {
        self.selectedImage = nil
        profilePic.image = nil
        profilePicView.isHidden = true
        selectPicBtn.isHidden = false
        if self.selectedTeacherData != nil {
            self.isPictureChanged = true
        }
    }
    
    @IBAction func schoolDropDownTap(_ sender: Any) {
        schoolDropDown.show()
    }
    @IBAction func selectBranchDropDownTap(_ sender: Any) {
        branchDropDown.show()
        
    }
    @IBAction func classDropDown(_ sender: Any) {
        classesDropDown.show()
    }
    
    @IBAction func genderDropDown(_ sender: Any) {
        genderDropDown.show()
    }
    
    func editOldTeacher(){
        if self.branchId == nil {
            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Please select a branch to continue.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            return
        }
        
        
        if self.usernameTF.text?.count == 0 || self.emailTF.text?.count == 0  || self.ageTF.text?.count == 0 || self.phoneNoTF.text?.count == 0 || self.genderTF.text?.count == 0 || self.classId == nil{
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        
        var classesArray : [[String:Any]] = [[String:Any]]()
        let classParam : [String:Any] = ["class_id": self.classId! , "is_class_teacher" : false]
        classesArray.append(classParam)        
        
        var teacherParams : [String : Any] = ["user_name" : usernameTF.text! ,
             "email" : emailTF.text!,
             "full_name" :ageTF.text!,
             "phone" :phoneNoTF.text!,
             "gender" :genderTF.text!,
             "branch_id" :self.branchId,
             "class_ids" :classesArray]
        if isPictureChanged == true {
            teacherParams["profile_picture_data"] = profilePic.image!.toBase64()!
        }
        let parameters : [String : Any] = ["auth_token" : auth! ,
            "teacher_id" : self.selectedTeacherData.id!,
            "TeacherData" : teacherParams]
        Utility.editTeacher(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let teacherDictDict = (response!["EditedTeacher"] as? [String:Any]){
                    if UserModel.deserialize(from: teacherDictDict) != nil {
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is AllTeachersViewController {
                            let topVC = UIApplication.topViewController() as! AllTeachersViewController
                            topVC.allTeachersAPI(page: 1)
                        }
                    }
                    else{
                        print("ISSUE IN DESERIALIZE")
                    }
                }
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Adding... \(Int(progress!*100.0))%", animated: true)
        })
    }
    
    
    func addnewTeacher()  {
        
        if self.usernameTF.text?.count == 0 || self.emailTF.text?.count == 0  || self.passwordTF.text?.count == 0 || self.ageTF.text?.count == 0 || self.phoneNoTF.text?.count == 0 || self.genderTF.text?.count == 0 || self.selectedImage == nil || self.selectedBranchId == nil || self.classId == nil{
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        
        
        
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        _ = profilePic.image!.jpegData(compressionQuality: 0.75)!
        teacherSendData.auth_token = auth
        teacherSendData.user_name = usernameTF.text
        teacherSendData.email = emailTF.text
        teacherSendData.password = passwordTF.text
        teacherSendData.age = "30"
        teacherSendData.full_name = ageTF.text

        teacherSendData.phone = phoneNoTF.text
        teacherSendData.gender = genderTF.text
        teacherSendData.is_notification = false
        teacherSendData.profile_picture_data = self.selectedImage.toBase64()
        teacherSendData.branch_id = self.selectedBranchId
        
        let obt = Assigned_classes()
        obt.class_id = self.classId
        obt.is_class_teacher = true
        teacherSendData.assigned_classes.append(obt)
        
        let parameters = teacherSendData.toJSON() //Optional for extra parameter

        Utility.addNewTeacher(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let teacherDictDict = (response!["NewTeacher"] as? [String:Any]){
                    if UserModel.deserialize(from: teacherDictDict) != nil {
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is AllTeachersViewController {
                            let topVC = UIApplication.topViewController() as! AllTeachersViewController
                            topVC.allTeachersAPI(page: 1)
                        }
                    }
                    else{
                        print("ISSUE IN DESERIALIZE")
                    }
                }
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Adding... \(Int(progress!*100.0))%", animated: true)
        })
    }
    func allSchoolsAPI(page: Int)  {
        SwiftSpinner.show("Fetching...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth, "page" : page, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchools)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        SwiftSpinner.hide()
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        self.schoolsResponse = dataObject
                        for i in 0..<self.schoolsResponse!.schools!.count {
                            self.totalSchoolResponse.append(self.schoolsResponse!.schools![i])
                            self.schoolDropDown.dataSource.append(self.totalSchoolResponse[i].schoolName!)
                        }
                        if self.schoolsResponse.nextPage == true {
                            self.initialValue1 = self.initialValue1 + 1
                            self.allSchoolsAPI(page: self.initialValue1)
                            
                        }
                        else {
                            
                            SwiftSpinner.hide()
                            
                        }
                        
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    
    func allSchoolBranchAPI()  {
        SwiftSpinner.show("Getting Branches...", animated: true)
        if schoolID == nil {
            return
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: [String : Any] = ["auth_token" : auth!, "school_id" : self.schoolID]
        
        
        Utility.getBranchesOfSchool(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let schoolDict = (response!["School"] as? [String:Any]) {
                    if let branchesDicts = (schoolDict["branches"] as? [[String:Any]]) {
                        self.allBranches.removeAll()
                        self.branchDropDown.dataSource.removeAll()
                        for branchesDict in branchesDicts {
                            if let branchObj = BranchModel.deserialize(from: branchesDict) {
                                self.allBranches.append(branchObj)
                                self.branchDropDown.dataSource.append(branchObj.branchName)
                            }
                            else{
                                print("ISSUE IN DESERIALIZE")
                            }
                        }
                    }
                }
            }
        }
    }
    func allClassesAPI(page: Int)  {
        SwiftSpinner.show("Fetching...")
        

        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        var params: Parameters = ["auth_token" : auth!, "page" : -1, "per_page" : 100]
        if schoolID != nil {
            params = ["auth_token" : auth!, "page" : -1, "per_page" : 100,"filters": [["filter_name": "school","filter_value": schoolID]]]
        }
        if selectedBranchId != nil {
            params = ["auth_token" : auth!, "page" : -1, "per_page" : 100,"filters": [["filter_name": "branch","filter_value": selectedBranchId]]]
        }

        
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allClasses)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllClassesResponse>().map(JSONString: json){
                        self.totalClassResponse.removeAll()
                        self.classesDropDown.dataSource.removeAll()
                        self.classesResponse = dataObject
                        for i in 0..<self.classesResponse!.classes!.count {
                            self.totalClassResponse.append(self.classesResponse!.classes![i])
                            self.classesDropDown.dataSource.append(self.totalClassResponse[i].className!)
                        }
                        
                    }
                }
            }
        }
    }
    
    func showAlertView() {
        let attributes = AlertsPopUps.bottomAlertAttributes
        
        // Generate textual content
        let title = EKProperty.LabelContent(text: "Photos", style: .init(font: MainFont.medium.with(size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "You can select Picture from Gallery or camera.", style: .init(font: MainFont.light.with(size: 13), color: .black, alignment: .center))
        let image = EKProperty.ImageContent(imageName: "imageIconLarge", size: CGSize(width: 25, height: 25), contentMode: .scaleAspectFit)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        
        // Generate buttons content
        let buttonFont = MainFont.medium.with(size: 16)
        
        // Close button
        let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Gray.a800)
        let closeButtonLabel = EKProperty.LabelContent(text: "Cancel", style: closeButtonLabelStyle)
        let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Gray.a800.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
        }
        
        // Remind me later Button
        let laterButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let laterButtonLabel = EKProperty.LabelContent(text: "Camera", style: laterButtonLabelStyle)
        let laterButton = EKProperty.ButtonContent(label: laterButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            
            let status = PHPhotoLibrary.authorizationStatus()
            
            switch status {
            case .authorized:
                DispatchQueue.main.async {
                    let cameraVc = UIImagePickerController()
                    cameraVc.sourceType = UIImagePickerController.SourceType.camera;
                    
                    cameraVc.delegate = self
                    cameraVc.allowsEditing = true
                    cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium
                    self.present(cameraVc, animated: true, completion: nil)
                }
                
            // show your media picker
            case .denied: break
            // probably alert the user that they need to grant photo access
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized {
                        DispatchQueue.main.async {
                            let cameraVc = UIImagePickerController()
                            cameraVc.sourceType = UIImagePickerController.SourceType.camera;
                            cameraVc.delegate = self
                            cameraVc.allowsEditing = true
                            cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium
                            self.present(cameraVc, animated: true, completion: nil)
                        }
                        
                    }
                })
            case .restricted: break
                // probably alert the user that photo access is restricted
            }
            
            
        }
        
        // Ok Button
        let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let okButtonLabel = EKProperty.LabelContent(text: "Gallery", style: okButtonLabelStyle)
        let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            DispatchQueue.main.async {
                let cameraVc = UIImagePickerController()
                cameraVc.sourceType = UIImagePickerController.SourceType.photoLibrary;
                
                cameraVc.delegate = self
                cameraVc.allowsEditing = true
                cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium
                self.present(cameraVc, animated: true, completion: nil)
            }
            
        }
        
        // Generate the content
        let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, laterButton, closeButton, separatorColor: EKColor.Gray.light, expandAnimatedly: true)
        
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        
        // Setup the view itself
        let contentView = EKAlertMessageView(with: alertMessage)
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
}
// MARK: - YangMingShan Delegate
extension AddNewTeacherViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImagePickerDelegate, YMSPhotoPickerViewControllerDelegate {
    
    
    
    // MARK: - ImagePicker Delegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        guard info[.mediaType] != nil else { return }
        let mediaType = info[.mediaType] as! CFString
        
        switch mediaType {
        case kUTTypeImage:
            let image = info[.originalImage] as? UIImage
            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
            self.selectedImage = resizedImage
            profilePic.image = resizedImage
            profilePicView.isHidden = false
            selectPicBtn.isHidden = true
            break
        case kUTTypeMovie:
            print("MOVIE SELECTED")
            
            break
        case kUTTypeLivePhoto:
            break
        default:
            break
        }
        
    }
    
    
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController){
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            
            for asset: PHAsset in photoAssets
            {
                switch asset.mediaType {
                case .image:
                    imageManager.requestImageData(for: asset, options: options) { data, _, _, _ in
                        if let data = data {
                            let image = UIImage(data: data)
                            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
                            self.selectedImage = resizedImage
                            self.profilePic.image = resizedImage
                            self.profilePicView.isHidden = false
                            self.selectPicBtn.isHidden = true
                            
                        }
                    }
                case .video: break
                    
                case .audio:
                    print("Audio")
                default:
                    print("Unknown")
                }
                
            }
            // Assign to Array with images
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        print("Imaged...")
    }
}
