//
//  AllAdminsViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 10/4/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//


import UIKit
import Alamofire
import ObjectMapper
import SwiftSpinner

class AllAdminsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var allTeachersTV: UITableView!
    
    @IBOutlet weak var addNewBtn: UIButton!
    
    
    
    
    
    var refreshControl = UIRefreshControl()
    
    var allAdmins : [UserModel] = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allTeachersTV.delegate = self
        allTeachersTV.dataSource = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.allTeachersTV.addSubview(refreshControl) // not required when using UITableViewController
        let userObj : UserModel = Utility.getUserFromPreference()!
        if userObj.type == "SchoolAdmin"{
            self.addNewBtn.isHidden = false
        }
        if userObj.type == "BranchAdmin"{
            self.addNewBtn.isHidden = true
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getAllAdmins()
    }
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        SwiftSpinner.show("Fetching...")
        self.getAllAdmins()  
    }
    
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func addTeacherBtnTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewAdminViewController") as? AddNewAdminViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allAdmins.count
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allTeachersTV.dequeueReusableCell(withIdentifier: "cell") as! AllTeachersTableViewCell
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        if userObj.type == "SchoolAdmin"{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        if userObj.type == "BranchAdmin"{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        
        
        
        
        let gottenAdmin = allAdmins[indexPath.row]
        
        
        cell.teacherImageView.setImage(url: URL(string: gottenAdmin.profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
        cell.teacherName.text = gottenAdmin.userName!
        cell.emailTF.text = gottenAdmin.email
        cell.phoneNoTF.text = gottenAdmin.phone
        cell.genderTF.text = gottenAdmin.gender
        cell.classNameTF.text = gottenAdmin.fullName
        
    
        
        cell.branchNameTF.text = gottenAdmin.branch?.branchName
        cell.schoolNameTF.text = gottenAdmin.branch?.school?.schoolName
        if gottenAdmin.type == "SchoolAdmin"{
            cell.schoolNameTF.text = gottenAdmin.school?.schoolName
        }
        
        
        cell.lastLoginTF.text = gottenAdmin.type
        
        
        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewAdminViewController") as? AddNewAdminViewController
            
           vc?.selectedAdmin = gottenAdmin
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        cell.deleteBtn = {
            self.removeTeachersAPI(teacher_id: gottenAdmin.id!)
            self.allAdmins.remove(at: indexPath.row)
            self.allTeachersTV.reloadData()
        }
        return cell
    }
    func getAllAdmins()  {
        if allAdmins.count == 0 {
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: [String : Any] = ["auth_token" : auth!, "page" : -1, "per_page" : 100]
        
        
        Utility.getAdmins(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let adminsDicts = (response!["Admins"] as? [[String:Any]]) {
                    self.allAdmins.removeAll()
                    for adminsDict in adminsDicts {
                        if let adminObj = UserModel.deserialize(from: adminsDict) {
                            self.allAdmins.append(adminObj)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                    self.allTeachersTV.reloadData()
                }
            }
        }
    }
    func removeTeachersAPI(teacher_id: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "teacher_id" : teacher_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeTeacher)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllTeachersResponseModel>().map(JSONString: json){

                        self.getAllAdmins()
                        print("removed")
                    }
                }
            }
        }
    }
    
    
    
}
