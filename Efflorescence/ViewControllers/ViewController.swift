//
//  ViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/16/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import UserNotifications


class ViewController: UIViewController {

    @IBOutlet weak var emailField: CustomTextField!
    @IBOutlet weak var passwordField: CustomTextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //self.emailField.text = "test-superadmin1"
        //self.emailField.text = "testparent1"
        //self.passwordField.text = "test"

       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let userObj : UserModel = Utility.getUserFromPreference(){
            if let _ = userObj.isPasswordPinChangeNeeded {
                if userObj.isPasswordPinChangeNeeded == false{
                    self.performSegue(withIdentifier: "ToPinLogin", sender: self)
                }
            }
        }
    }
    
    
    @IBAction func loginClicked(_ sender: UIButton){
        if self.emailField.text?.count == 0 || self.passwordField.text?.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill Username and Password.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
       
        SwiftSpinner.show("Validating...")
        let parameters : [String : Any] = ["user_name" : self.emailField.text! ,
                                           "password" : self.passwordField.text!]
        print(parameters)
        Utility.login(withParameters: parameters) { (status, error, response) in
            if status == false {
                print("ERROR = \(error!)")
                SwiftSpinner.hide()
                AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let userDict = (response!["User"] as? [String : Any]){
                    if let userObj = UserModel.deserialize(from: userDict) {
                        print("User Name = \(userObj.userName!)")
                        let center = UNUserNotificationCenter.current()
                        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                            // Enable or disable features based on authorization.
                        }
                        UIApplication.shared.registerForRemoteNotifications()
                        
                        
                        
                        
                        Utility.saveUserInPreference(User: userObj)
                        if userObj.isPasswordPinChangeNeeded == true{
                            self.performSegue(withIdentifier: "ToSetPassword", sender: self)
                        }
                        else{
                            self.performSegue(withIdentifier: "ToTimeline", sender: self)
                        }
                        SwiftSpinner.hide()
                    }
                }
            }
        }
    }
}

