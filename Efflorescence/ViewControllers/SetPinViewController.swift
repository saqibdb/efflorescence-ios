//
//  SetPinViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/16/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SVPinView
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import UserNotifications


class SetPinViewController: UIViewController {

    @IBOutlet weak var pinField: CustomTextField!

    @IBOutlet weak var pinView: SVPinView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func nextClicked(_ sender: UIButton){
        if self.pinView.getPin().count < 6 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill Pin Completely.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        SwiftSpinner.show("Updating...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let parameters : [String : Any] = ["auth_token" : userObj.authToken ,
                                           "pin" : self.pinView.getPin()]
        
        print(parameters)
        Utility.setPin(withParameters: parameters) { (status, error, response) in
            if status == false {
                print("ERROR = \(error!)")
                SwiftSpinner.hide()
                AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                SwiftSpinner.hide()
                if let status = (response!["Status"] as? String){
                    if status == "Pin has been made."{

                        if let userDict = (response!["User"] as? [String : Any]){
                            if let userObj = UserModel.deserialize(from: userDict) {
                                print("User Name = \(userObj.userName!)")
                                let center = UNUserNotificationCenter.current()
                                center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                                    // Enable or disable features based on authorization.
                                }
                                UIApplication.shared.registerForRemoteNotifications()
                                
                                Utility.saveUserInPreference(User: userObj)
                                self.performSegue(withIdentifier: "ToTimeline", sender: self)
                            }
                        }
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
