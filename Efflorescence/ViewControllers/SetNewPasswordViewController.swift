//
//  SetNewPasswordViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/16/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON



class SetNewPasswordViewController: UIViewController {

    
    @IBOutlet weak var passwordField: CustomTextField!
    @IBOutlet weak var rePasswordField: CustomTextField!

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func nextClicked(_ sender: UIButton){
        //self.performSegue(withIdentifier: "ToSetPin", sender: self)
        
        if self.passwordField.text?.count == 0 || self.rePasswordField.text?.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill Password and Re Password.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        if self.passwordField.text != self.rePasswordField.text {
            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Both Passwords does not match!", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        
        SwiftSpinner.show("Updating...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let parameters : [String : Any] = ["auth_token" : userObj.authToken ,
                                           "password" : self.passwordField.text!]
    
        print(parameters)
        Utility.setPassword(withParameters: parameters) { (status, error, response) in
            if status == false {
                print("ERROR = \(error!)")
                SwiftSpinner.hide()
                AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                SwiftSpinner.hide()
                if let status = (response!["Status"] as? String){
                    if status == "Password has been changed."{
                        self.performSegue(withIdentifier: "ToSetPin", sender: self)
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
            }
        }
    }
        
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
