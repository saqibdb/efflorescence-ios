//
//  AddNewPostViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/26/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import DateToolsSwift
import Imaginary
import DropDown
import SwiftEntryKit
import ImagePicker
import Foundation
import MobileCoreServices
import YangMingShan
import SimpleImageViewer
import TagListView


class AddNewPostViewController: UIViewController {

    @IBOutlet weak var classNameText: UILabel!
    
    @IBOutlet weak var classFilterBtn: UIButton!
    
    @IBOutlet weak var childNameText: UILabel!
    
    @IBOutlet weak var childFilterBtn: UIButton!
    
    @IBOutlet weak var postTitleText: UITextField!
    
    @IBOutlet weak var postDescription: UITextView!
    
    @IBOutlet weak var mediaCollection: UICollectionView!
    
    
    @IBOutlet weak var selectClassLabel: UILabel!
    
    @IBOutlet weak var selectChildLabel: UILabel!
    
    
    
    var allClasses : [ClassModel] = [ClassModel]()
    var selectedClass : ClassModel!
    
    
    var allStudents : [UserModel] = [UserModel]()
    var selectedStudent : [UserModel] = [UserModel]()
    
    
    var allSubjects : [SubjectModel] = [SubjectModel]()
    var selectedSubjects : [SubjectModel] = [SubjectModel]()
    
    
    
    let classesDropDown = DropDown()
    let studentDropDown = DropDown()

    let subjetcsDropDown = DropDown()

    
    var selectedMedias : [Any] = [Any]()
    

    
    @IBOutlet weak var selectedStudentsTagsView: TagListView!
    
    @IBOutlet weak var selectedSubjectsTagsView: TagListView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // The view to which the drop down will appear on
        classesDropDown.anchorView = classFilterBtn // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        classesDropDown.dataSource = []
        
        
        // Action triggered on selection
        classesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.classesDropDown.hide()
            self.classNameText.text = item
            self.selectedClass = self.allClasses[index]
            
            
            self.subjetcsDropDown.dataSource.removeAll()
            for i in 0..<self.selectedClass!.subjects!.count{
                let subject = self.selectedClass!.subjects![i]
                self.subjetcsDropDown.dataSource.append(subject.subjectName)
            }

        }
        // Will set a custom width instead of the anchor view width
        classesDropDown.width = 350
        
        
        
        // The view to which the drop down will appear on
        studentDropDown.anchorView = childFilterBtn // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        studentDropDown.dataSource = []
        
        
        // Action triggered on selection
        studentDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.studentDropDown.hide()
            self.childNameText.text = item
        }
        // Will set a custom width instead of the anchor view width
        studentDropDown.width = 200
        
        
        
        // The view to which the drop down will appear on
        subjetcsDropDown.anchorView = selectedSubjectsTagsView    // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        subjetcsDropDown.dataSource = []
        
        
        // Action triggered on selection
        subjetcsDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.subjetcsDropDown.hide()
            
            let selectedSubject = self.selectedClass!.subjects![index]
            if self.selectedSubjects.contains(where: { subject in subject.id == selectedSubject.id }) {
                print("1 exists in the array")
            } else {
                self.selectedSubjectsTagsView.addTag(item)
                self.selectedSubjects.append(selectedSubject)
            }
            
        }
        // Will set a custom width instead of the anchor view width
        subjetcsDropDown.width = 200
        
        
        
        
        
        
        
        self.mediaCollection.delegate = self
        self.mediaCollection.dataSource = self
        self.selectedStudentsTagsView.delegate = self
        self.selectedStudentsTagsView.tag = 1
        
        self.selectedSubjectsTagsView.delegate = self
        self.selectedSubjectsTagsView.tag = 2
        
        
        let userObj : UserModel! = Utility.getUserFromPreference()

        if userObj.type == "Parent" {
            self.selectClassLabel.text = "Select Child"
            self.selectChildLabel.text = "Select Teachers"
            self.classNameText.text = "Child (Class)"
            self.childNameText.text = "0 Teachers Selected"
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        getClasses()
        //getSubjects()
    }

    @IBAction func classFilterAction(_ sender: UIButton) {
        classesDropDown.show()
    }
    
    @IBAction func childFilterAction(_ sender: UIButton) {
        //studentDropDown.show()
        if self.selectedClass != nil {
            self.performSegue(withIdentifier: "ToStudentSelection", sender: self.selectedClass)
        }
        else{
            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Please select a Class before selecting students", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)

        }

    }
    
    
    @IBAction func subjectFilterAction(_ sender: UIButton) {
        subjetcsDropDown.show()
    }
    
    
    
    
    func createNewPost(withTags tags :[[String:Any]], andMedias medias : [[String:Any]], andSubjects subjects : [[String:Any]]) {
        
        SwiftSpinner.show("Finalizing...", animated: true)
        let userObj : UserModel! = Utility.getUserFromPreference()
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_title" : self.postTitleText.text!,
                                           "post_description" : self.postDescription.text!,
                                           "class_id":self.selectedClass.id,
                                           "tag_users_ids":tags,
                                           "medias":medias,
                                           "subjects":subjects]
        Utility.addPostWithMedia(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let postsDict = (response!["NewPost"] as? [String:Any]){
                    if let postsObj = PostModel.deserialize(from: postsDict) {
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is TimelineViewController {
                            let topVC = UIApplication.topViewController() as! TimelineViewController
                            topVC.allPosts.removeAll()
                        }
                    }
                    else{
                        print("ISSUE IN DESERIALIZE")
                    }
                }
            }
        }, andProgressHandler: { (progress) in
            SwiftSpinner.show("Posting... \(Int(progress!*100.0))%", animated: true)
        })
    }
    
    
    
    
    @IBAction func publishAction(_ sender: UIButton) {
        postTitleText.endEditing(true)
        postDescription.endEditing(true)
        
        if self.postTitleText.text?.count == 0 || self.postDescription.text?.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
            return;
        }
        
        

        
        if postTitleText.text?.count != 0 && postDescription.text?.count != 0 {
            SwiftSpinner.show("Posting...", animated: true)
            
            

            var tagIdDataArray : [[String:Any]] = [[String:Any]]()
            var mediaDataArray : [[String:Any]] = [[String:Any]]()
            var subjectsArray : [[String:Any]] = [[String:Any]]()

            for student in self.selectedStudent {
                let tagParam : [String:Any] = ["id": student.id]
                tagIdDataArray.append(tagParam)
            }
            for subject in self.selectedSubjects {
                let tagParam : [String:Any] = ["subject_id": subject.id]
                subjectsArray.append(tagParam)
            }
           
            var checker = 0
            var gotError : Bool = false
            if self.selectedMedias.count == 0 {
                self.createNewPost(withTags: tagIdDataArray, andMedias: mediaDataArray, andSubjects: subjectsArray)
            }
            else{
                for media in self.selectedMedias{
                    var mediaStr = ""
                    var mediaType = "image"
                    
                    var data : Data!
                    var type : Utility.DataType = .photo
                    
                    if media is UIImage {
                        mediaStr = (media as! UIImage).toBase64()!
                        mediaType = "image"
                        
                        data = (media as! UIImage).jpegData(compressionQuality: 0.9)
                        type = .photo
                        checker = checker + 1
                    }
                    
                    if media is URL {
                        if let videoData = try? NSData(contentsOf: media as! URL, options: .mappedIfSafe) {
                            mediaStr = videoData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                            mediaType = "video"
                            data = videoData as Data
                            type = .video
                            checker = checker + 1
                        }
                    }
                    //SwiftSpinner.show("Posting...", animated: true)

                    Utility.uploadToAmazonS3(withData: data, andDataType: type, andResponseHandler: { (status, error, response) in
                        if status == false {
                            checker = checker - 1
                            print(error!)
                            gotError = true
                        }
                        else{
                            checker = checker - 1
                            print("Response = \(String(describing: response!["url"]!))")
                            let mediaParam : [String:Any] = ["media_link" : response!["url"]! as! String,
                                                             "media_type" : mediaType]
                            mediaDataArray.append(mediaParam)
                        }
                        
                        if checker == 0 {
                            if gotError == true {
                                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Please try again", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                                SwiftSpinner.hide()
                                return
                            }
                            self.createNewPost(withTags: tagIdDataArray, andMedias: mediaDataArray, andSubjects: subjectsArray)

                        }
                    }) { (progress) in
                    }
                }
            }
        }
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    func getClasses(){
        if allClasses.count == 0{
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth!]
        
        
        Utility.getTeacherClasses(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            

            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let classesDicts = (response!["Classes"] as? [[String:Any]]){
                    self.allClasses.removeAll()
                    self.classesDropDown.dataSource.removeAll()
                    for classDict in classesDicts {
                        if let classsObj = ClassModel.deserialize(from: classDict) {
                            self.allClasses.append(classsObj)
                            self.classesDropDown.dataSource.append(classsObj.className)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                }
            }
        }
    }
    
    func getSubjects(){
        if allSubjects.count == 0{
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth!]
        
        
        Utility.getSubjects(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let subjectsDicts = (response!["Subjects"] as? [[String:Any]]){
                    self.allSubjects.removeAll()
                    self.subjetcsDropDown.dataSource.removeAll()
                    for subjectDict in subjectsDicts {
                        if let subjectObj = SubjectModel.deserialize(from: subjectDict) {
                            self.allSubjects.append(subjectObj)
                            self.subjetcsDropDown.dataSource.append(subjectObj.subjectName)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                }
            }
        }
    }
    
    
    func showAlertView() {
        let attributes = AlertsPopUps.bottomAlertAttributes
        
        // Generate textual content
        let title = EKProperty.LabelContent(text: "Photos", style: .init(font: MainFont.medium.with(size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "You can select Photos from Gallery or camera.", style: .init(font: MainFont.light.with(size: 13), color: .black, alignment: .center))
        let image = EKProperty.ImageContent(imageName: "imageIconLarge", size: CGSize(width: 25, height: 25), contentMode: .scaleAspectFit)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        
        // Generate buttons content
        let buttonFont = MainFont.medium.with(size: 16)
        
        // Close button
        let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Gray.a800)
        let closeButtonLabel = EKProperty.LabelContent(text: "Cancel", style: closeButtonLabelStyle)
        let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Gray.a800.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
        }
        
        // Remind me later Button
        let laterButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let laterButtonLabel = EKProperty.LabelContent(text: "Camera", style: laterButtonLabelStyle)
        let laterButton = EKProperty.ButtonContent(label: laterButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            //            let imagePickerController =
            //            imagePickerController.delegate = self
            //            self.present(imagePickerController, animated: true, completion: nil)
            let status = PHPhotoLibrary.authorizationStatus()
            
            switch status {
            case .authorized:
                let cameraVc = UIImagePickerController()
                cameraVc.sourceType = .camera
                cameraVc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                
                
                cameraVc.delegate = self
                cameraVc.allowsEditing = true
                cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium
                self.present(cameraVc, animated: true, completion: nil)
            // show your media picker
            case .denied: break
            // probably alert the user that they need to grant photo access
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized {
                        let cameraVc = UIImagePickerController()
                        cameraVc.sourceType = .camera
                        cameraVc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                        
                        
                        cameraVc.delegate = self
                        cameraVc.allowsEditing = true
                        cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium
                        self.present(cameraVc, animated: true, completion: nil)
                    }
                })
            case .restricted: break
                // probably alert the user that photo access is restricted
            }
            
            
        }
        
        // Ok Button
        let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let okButtonLabel = EKProperty.LabelContent(text: "Gallery", style: okButtonLabelStyle)
        let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            let pickerViewController = YMSPhotoPickerViewController.init()
            pickerViewController.numberOfPhotoToSelect = 5
            self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
            
        }
        
        // Generate the content
        let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, laterButton, closeButton, separatorColor: EKColor.Gray.light, expandAnimatedly: true)
        
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        
        // Setup the view itself
        let contentView = EKAlertMessageView(with: alertMessage)
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    
    @objc func removeItem(sender: UIButton!) {
        self.selectedMedias.remove(at: sender.tag)
        self.mediaCollection.reloadData()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if sender is ClassModel {
            let vc = segue.destination as! StudentsSelectionViewController
            vc.selectedClass = (sender as! ClassModel)
            vc.delegate = self
        }
    }
 

}
extension AddNewPostViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedMedias.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell!
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddMediaCollectionViewCell", for: indexPath) as! AddMediaCollectionViewCell
        
        
        if indexPath.row != self.selectedMedias.count {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionViewCell", for: indexPath) as! MediaCollectionViewCell
            
            let media = self.selectedMedias[indexPath.row]
            if media is UIImage{
                (cell as! MediaCollectionViewCell).mainImage.image = (media as! UIImage)
                (cell as! MediaCollectionViewCell).mainImage.clipsToBounds = true
                (cell as! MediaCollectionViewCell).removeImage.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
                (cell as! MediaCollectionViewCell).removeImage.tag = indexPath.row
            }
            
            if media is URL{
                Utility.captureThumbnail(withVideoURL: media as! URL, secs: 0, preferredTimeScale: 1) { (image) in
                    DispatchQueue.main.async {
                        (cell as! MediaCollectionViewCell).mainImage.image = image
                        cell.layoutIfNeeded()
                    }
                }
                
                (cell as! MediaCollectionViewCell).mainImage.clipsToBounds = true
                (cell as! MediaCollectionViewCell).removeImage.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
                (cell as! MediaCollectionViewCell).removeImage.tag = indexPath.row
            }
            
        }
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == self.selectedMedias.count {
            self.showAlertView()
        }
    }
}

// MARK: - StudentsSelection Delegate

extension AddNewPostViewController : StudentsSelectionDelegate {
    func studentsSelectionCompleted(selectedStudents: [UserModel]) {
        self.selectedStudent = selectedStudents
        self.childNameText.text = "\(self.selectedStudent.count) students selected."
        let userObj : UserModel! = Utility.getUserFromPreference()
        if userObj.type == "Parent" {
            self.childNameText.text = "\(self.selectedStudent.count) teachers selected."
        }
        self.selectedStudentsTagsView.removeAllTags()
        
        for student in self.selectedStudent {
            self.selectedStudentsTagsView.addTag(student.userName)
        }
    }
    
}

extension AddNewPostViewController : TagListViewDelegate {
    @objc func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void{
        if sender.tag == 1{
            self.selectedStudent = self.selectedStudent.filter{ $0.userName != title }
            sender.removeTag(title)
            self.childNameText.text = "\(self.selectedStudent.count) students selected."
            let userObj : UserModel! = Utility.getUserFromPreference()
            if userObj.type == "Parent" {
                self.childNameText.text = "\(self.selectedStudent.count) teachers selected."
            }
        }
        if sender.tag == 2{
            self.selectedSubjects = self.selectedSubjects.filter{ $0.subjectName != title }
            sender.removeTag(title)
        }
        
    }
}



// MARK: - YangMingShan Delegate
extension AddNewPostViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImagePickerDelegate, YMSPhotoPickerViewControllerDelegate {
    
    
    
    // MARK: - ImagePicker Delegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        guard info[.mediaType] != nil else { return }
        let mediaType = info[.mediaType] as! CFString
        
        switch mediaType {
        case kUTTypeImage:
            let image = info[.originalImage] as? UIImage
            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
            self.selectedMedias.append(resizedImage)
            self.mediaCollection.reloadData()
            break
        case kUTTypeMovie:
            print("MOVIE SELECTED")
            let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            self.selectedMedias.append(videoURL!)
            self.mediaCollection.reloadData()

            
            break
        case kUTTypeLivePhoto:
            break
        default:
            break
        }
        
    }

    
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController){
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            
            for asset: PHAsset in photoAssets
            {
                switch asset.mediaType {
                case .image:
                    imageManager.requestImageData(for: asset, options: options) { data, _, _, _ in
                        if let data = data {
                            let image = UIImage(data: data)
                            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
                            self.selectedMedias.append(resizedImage)
            
                        }
                    }
                case .video:
                    asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
                        
                        if ((contentEditingInput!.audiovisualAsset as? AVURLAsset)?.url) != nil {
                            PHImageManager.default().requestAVAsset(forVideo: asset, options: PHVideoRequestOptions(), resultHandler: { (assetNew, audioMix, info) -> Void in
                                if let assetNewOne = assetNew as? AVURLAsset {
                                    
                                    let videoURL = assetNewOne.url
                                    self.selectedMedias.append(videoURL)
                                    DispatchQueue.main.async {
                                        self.mediaCollection.reloadData()
                                    }
                                }
                            })
                        }
                    })
                case .audio:
                    print("Audio")
                default:
                    print("Unknown")
                }
                
                
                
                self.mediaCollection.reloadData()
                
            }
            // Assign to Array with images
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        print("Imaged...")
    }
}


