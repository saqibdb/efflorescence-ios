//
//  StudentsSelectionViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 5/13/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//


import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import Imaginary
import SwiftEntryKit
import SimpleImageViewer


protocol StudentsSelectionDelegate:class {
    func studentsSelectionCompleted(selectedStudents: [UserModel])
}


class StudentsSelectionViewController: UIViewController {
    weak var delegate: StudentsSelectionDelegate?
    
    var selectedClass : ClassModel!
    
    
    var allStudents : [UserModel] = [UserModel]()
    var selectedStudents :  [UserModel] = [UserModel]()
    
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    
    
    @IBOutlet weak var titleText: UILabel!
    
    
    
    @IBOutlet weak var addSelectedBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        self.mainCollectionView.allowsMultipleSelection = true
        
        let userObj : UserModel! = Utility.getUserFromPreference()
        if userObj.type == "Parent" {
            self.titleText.text = "Add Teachers"
            self.addSelectedBtn.setTitle("Add Selected Teachers", for: .normal)
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.getStudentOfClass(withId: self.selectedClass.id)
    }
    
    func getStudentOfClass(withId classId : Int){
        SwiftSpinner.show("Fetching Students...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth!,
                                           "class_id" : classId]
        
        
        Utility.getStudentOfClass(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let studentsDicts = (response!["Students"] as? [[String:Any]]){
                    self.allStudents.removeAll()
                    for studentDict in studentsDicts {
                        if let studentObj = UserModel.deserialize(from: studentDict) {
                            self.allStudents.append(studentObj)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                    self.mainCollectionView.reloadData()
                }
            }
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func selectedAction(_ sender: UIButton) {
        if self.delegate != nil {
            self.delegate?.studentsSelectionCompleted(selectedStudents: self.selectedStudents)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension StudentsSelectionViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allStudents.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentCollectionViewCell", for: indexPath) as! StudentCollectionViewCell
        cell.selectAllView.isHidden = false
        if indexPath.row > 0{
            cell.selectAllView.isHidden = true
            let student = self.allStudents[indexPath.row-1]
            //cell.studentName.text = student.userName
            
            cell.studentProfilePictureView.setImage(url: URL(string: student.profilePictureURL)!, placeholder: UIImage(named:"noImage"))
            cell.studentProfilePictureView.setImage(url: URL(string: student.profilePictureURL)!) { result in
                switch result {
                case .value(let image):
                    cell.studentProfilePictureView.image = image
                case .error(let error):
                    cell.studentProfilePictureView.image = UIImage(named:"noImage")
                    print("Image Load error = \(error.localizedDescription)")
                }
                cell.layoutIfNeeded()
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            for row in 0..<collectionView.numberOfItems(inSection: 0) {
                collectionView.deselectItem(at: IndexPath(row: row, section: 0), animated: false)
            }
            self.selectedStudents.removeAll()
        }
        else{
            let student = self.allStudents[indexPath.row-1]
            self.selectedStudents = self.selectedStudents.filter{ $0.id != student.id }

        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.deselectItem(at: indexPath, animated: true)
        let cell =  collectionView.cellForItem(at: indexPath) as! StudentCollectionViewCell
        if indexPath.row == 0 {
            if cell.isSelected == true {
                for row in 0..<collectionView.numberOfItems(inSection: 0) {
                    collectionView.selectItem(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: [])
                }
                self.selectedStudents = self.allStudents
            }
            
            
        }
        else{
            let student = self.allStudents[indexPath.row-1]
            if cell.isSelected == true {
                self.selectedStudents.append(student)
            }
        }
        
    }
}
