//
//  AddNewBranchViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 24/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper
import Alamofire
import SwiftSpinner


class AddNewBranchViewController: UIViewController {
    @IBOutlet weak var classFilterBtn: UIButton!
    @IBOutlet weak var schoolNameText: UITextField!
    @IBOutlet weak var schoolAddressTF: UITextField!
    @IBOutlet weak var branchNameTF: UITextField!
    @IBOutlet weak var headerLbl: UILabel!
    
    let classesDropDown = DropDown()
    var schoolsResponse: AllSchoolResponse!
    var totalResponse = [Schools]()
    var initialValue = 1
    var selectedSchool : Schools!
     var edit = false
    var selectedBranch: Branches!
    
    
    var selectedSchoolID: Int!
    var branchName: String!
    var branchAddress: String!
    var schoolName: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        if edit == true {
            headerLbl.text = "Edit Branch"
            schoolNameText.text = schoolName
            schoolAddressTF.text = branchName
            branchNameTF.text = branchAddress
        }
         allSchoolsAPI(page: initialValue)
        classesDropDown.anchorView = classFilterBtn
        
        classesDropDown.dataSource = []
        classesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.classesDropDown.hide()
            self.schoolNameText.text = item
            self.selectedSchool = self.totalResponse[index]
            self.selectedSchoolID = self.selectedSchool.id!
            
        }
        // Will set a custom width instead of the anchor view width
        classesDropDown.width = 200

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
          _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func saveBtnTap(_ sender: Any) {
        if schoolNameText.text == "" || schoolAddressTF.text == "" || branchNameTF.text == "" {
            let alert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            if edit == true {
                if self.selectedBranch == nil || self.selectedSchoolID == nil {
                    AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
                    return;
                }
                
                
                
                
                editBranch(branch_id: self.selectedBranch.id!, branch_name: schoolAddressTF.text!, branch_address: branchNameTF.text!, school_id: selectedSchoolID)
            }else {
                if self.selectedSchoolID == nil {
                    AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
                    return;
                }
                addNewBranch(branch_name: schoolAddressTF.text!, branch_address: branchNameTF.text!, school_id: self.selectedSchoolID)
            }
            
        }
        
    }
    
    @IBAction func classFilterAction(_ sender: UIButton) {
        classesDropDown.show()
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    
    func allSchoolsAPI(page: Int)  {
        SwiftSpinner.show("Fetching...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth, "page" : page, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchools)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        self.schoolsResponse = dataObject
                        for i in 0..<self.schoolsResponse!.schools!.count {
                            self.totalResponse.append(self.schoolsResponse!.schools![i])
                        }
                        if self.schoolsResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allSchoolsAPI(page: self.initialValue)
                            
                        }
                        else {
                            for i in 0..<self.totalResponse.count{
                                self.classesDropDown.dataSource.append(self.totalResponse[i].schoolName!)
                            }
                            
                            for i in 0..<self.totalResponse.count{
                                if self.totalResponse[i].schoolName == self.schoolName
                                {
                                    self.selectedSchoolID = self.totalResponse[i].id
                                }
                            }
                           
                            
                            SwiftSpinner.hide()
//                            self.allSchoolsTableView.reloadData()
                        }
                        
                        
                        
                        
                        
                    }
                }
            }
        }
    }

    
    func addNewBranch(branch_name: String, branch_address: String,school_id: Int)  {
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "branch_name" : branch_name, "branch_address":branch_address,"school_id":school_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addBranch)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error != nil {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                            self.schoolNameText.text = ""
                            self.schoolNameText.placeholder = "School Name"
                            self.schoolAddressTF.text = ""
                            self.schoolAddressTF.placeholder = "School Address"
                            self.branchNameTF.text = ""
                            self.branchNameTF.placeholder = "Branch names"
                            let alert = UIAlertController(title: "Add", message: "New School Added", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            
                            //self.present(alert, animated: true, completion: nil)
                            _ = self.navigationController?.popViewController(animated: true)

                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    func editBranch(branch_id: Int, branch_name: String, branch_address: String,school_id: Int)  {
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        
        
        let params: Parameters = ["auth_token" : auth!, "branch_id" : branch_id, "BranchData":["branch_name":branch_name,"branch_address":branch_address,"school_id":school_id]]
        
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.editBranch)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error == "There is already a school present with this name and address" {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                            _ = self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }


}
