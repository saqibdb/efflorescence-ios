//
//  TimelineViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/19/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import DateToolsSwift
import Imaginary
import DatePicker
import DropDown
import FAPanels

class TimelineViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    var refreshControl = UIRefreshControl()

    
    @IBOutlet weak var addPostBtn: UIButton!
    
    var allPosts : [PostModel] = [PostModel]()
    
    
    
    @IBOutlet weak var dateFilterBtn: UIButton!
    
    @IBOutlet weak var dateFilterText: UILabel!
    
    @IBOutlet weak var secondFilterTitle: UILabel!
    
    @IBOutlet weak var secondFilterText: UILabel!
    
    

    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    @IBOutlet weak var datePickerConstraint: NSLayoutConstraint!
    
    
    
    
    
    
    
    
    
    
    let dropDown = DropDown()
    
    
    var selectedFilters : [[String:Any]] = [[String:Any]]()

    
    
    
    var allClasses : [ClassModel] = [ClassModel]()
    var selectedClass : ClassModel!
    
    var allChild : [UserModel] = [UserModel]()
    var selectedChild : UserModel!
    
    var selectedDate : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)

        
        self.mainTableView.addSubview(refreshControl) // not required when using UITableViewController

        self.datePicker.maximumDate = Date()
    }
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()

        SwiftSpinner.show("Fetching...")
        getAllPosts()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.refreshControl.endRefreshing()
        
        
        
        //let crashArray : [String] = [String]()
        //let itemCrashed = crashArray[1]
        
        
        
        
    }
    
    
    func setupFilters(){
        if self.selectedDate == nil {
            self.dateFilterText.text = "Any Date"
        }
        
        
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        
        // The view to which the drop down will appear on
        dropDown.anchorView = secondFilterText // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = []
        
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.secondFilterText.text = item
            self.dropDown.hide()
            self.selectedFilters.removeAll()
            if index == 0 {
                
            }
            else{
                if userObj.type == "Parent" {
                    self.selectedClass = nil
                    self.selectedChild = self.allChild[index-1]
                }
                else{
                    self.selectedClass = self.allClasses[index-1]
                    self.selectedChild = nil
                }
                if self.selectedClass != nil {
                    let classFilter : [String : Any] = ["filter_name" : "class",
                                                        "filter_value" : self.selectedClass.id]
                    self.selectedFilters.append(classFilter)
                }
                
                if self.selectedChild != nil {
                    let classFilter : [String : Any] = ["filter_name" : "child",
                                                        "filter_value" : self.selectedChild.id]
                    self.selectedFilters.append(classFilter)
                }
            }
            
            
            if self.selectedDate != nil {
                let classFilter : [String : Any] = ["filter_name" : "date",
                                                    "filter_value" : "\(self.selectedDate.year)-\(self.selectedDate.month)-\(self.selectedDate.day)"]
                self.selectedFilters.append(classFilter)
            }
            self.allPosts.removeAll()
            self.getAllPosts()
        }
        
        // Will set a custom width instead of the anchor view width
        dropDown.width = 200
        
        
        
       
        
        if userObj.type == "Parent" {
            //self.addPostBtn.isHidden = true
            self.secondFilterTitle.text = "Select Child"
            if self.selectedChild == nil {
                self.secondFilterText.text = "Any Child"
            }
            if allChild.count == 0{
                SwiftSpinner.show("Fetching...")
            }
            let auth = userObj.authToken
            let parameters : [String : Any] = ["auth_token" : auth!,
                                               "parent_id" : userObj.id]
            
            
            Utility.getChildOfParent(withParameters: parameters) { (status, error, response) in
                SwiftSpinner.hide()
                
                
                if status == false {
                    AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                }
                else{
                    if let childrenDicts = (response!["Children"] as? [[String:Any]]){
                        self.dropDown.dataSource.removeAll()
                        self.dropDown.dataSource.append("Any Child")
                        self.allChild.removeAll()
                        for childrenDict in childrenDicts {
                            if let childrenObj = UserModel.deserialize(from: childrenDict) {
                                self.allChild.append(childrenObj)
                                self.dropDown.dataSource.append(childrenObj.userName)
                            }
                            else{
                                print("ISSUE IN DESERIALIZE")
                            }
                        }
                    }
                }
            }
        }
        else{
            self.addPostBtn.isHidden = false
            if self.selectedClass == nil {
                self.secondFilterText.text = "Any Class"
            }
            self.secondFilterTitle.text = "Select Class"
            let auth = userObj.authToken
            let parameters : [String : Any] = ["auth_token" : auth!]
            if allClasses.count == 0{
                SwiftSpinner.show("Fetching...")
            }
            Utility.getTeacherClasses(withParameters: parameters) { (status, error, response) in
                SwiftSpinner.hide()
                
                if status == false {
                    AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                }
                else{
                    if let classesDicts = (response!["Classes"] as? [[String:Any]]){
                        self.allClasses.removeAll()
                        self.dropDown.dataSource.removeAll()
                        self.dropDown.dataSource.append("Any Class")
                        for classDict in classesDicts {
                            if let classsObj = ClassModel.deserialize(from: classDict) {
                                self.allClasses.append(classsObj)
                                self.dropDown.dataSource.append(classsObj.className)
                            }
                            else{
                                print("ISSUE IN DESERIALIZE")
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func dateFilterUpdate(){
        self.selectedFilters.removeAll()
        if self.selectedDate == nil {
            self.dateFilterText.text = "Any Date"
        }
        else{
            self.dateFilterText.text = "\(selectedDate.year)-\(selectedDate.month)-\(selectedDate.day)"//selectedDate.string()
            
            let classFilter : [String : Any] = ["filter_name" : "date",
                                                "filter_value" : "\(self.selectedDate.year)-\(self.selectedDate.month)-\(self.selectedDate.day)"]
            self.selectedFilters.append(classFilter)
        }
        
        
        if self.selectedClass != nil {
            let classFilter : [String : Any] = ["filter_name" : "class",
                                                "filter_value" : self.selectedClass.id]
            self.selectedFilters.append(classFilter)
        }
        
        if self.selectedChild != nil {
            let classFilter : [String : Any] = ["filter_name" : "child",
                                                "filter_value" : self.selectedChild.id]
            self.selectedFilters.append(classFilter)
        }
        
        self.allPosts.removeAll()
        self.getAllPosts()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        self.datePickerConstraint.constant = 150
        setupFilters()
        getAllPosts()
        
    }
    
    @IBAction func dateFilterAction(_ sender: UIButton) {
        
        self.datePickerConstraint.constant = 0

        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func childFilterAction(_ sender: UIButton) {
        dropDown.show()

    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    @IBAction func addPostAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToAddPost", sender: self)
    }
    
    
    @IBAction func anyDateAction(_ sender: UIButton) {
        self.datePickerConstraint.constant = 150
        self.view.layoutIfNeeded()
        self.selectedDate = nil
        self.dateFilterUpdate()

    }
    
    @IBAction func selectDateAction(_ sender: UIButton) {
        self.datePickerConstraint.constant = 150
        self.view.layoutIfNeeded()

        self.selectedDate = self.datePicker.date
        self.dateFilterUpdate()
    }
    
    
    
    
    
    
    
    
    @objc func deletePostAction(sender: UIButton!) {
        
        let post = self.allPosts[sender.tag]        
        let alert = UIAlertController(title: app_Name, message: "Are you sure you want to delete this Post?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.removePostFromTimeLine(withPostId: post.id)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func editPostAction(sender: UIButton!) {
        let post = self.allPosts[sender.tag]
        self.performSegue(withIdentifier: "ToEditPost", sender: post)
    }
    
    @objc func likePostAction(sender: UIButton!) {
        let post = self.allPosts[sender.tag]
        SwiftSpinner.show("Updating...", animated: true)
        let userObj : UserModel! = Utility.getUserFromPreference()
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_id" : post.id]
        Utility.addRemoveLikeForPost(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let likesCount = (response!["total_likes"] as? Int){
                    post.total_likes = likesCount
                    self.mainTableView.reloadData()
                    
                }
            }
        })
        
        
    }
    
    func removePostFromTimeLine(withPostId postId : Int){
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_id" : postId]
        SwiftSpinner.show("Removing...")

        Utility.removePostFromTimeline(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let status = (response!["Status"] as? String){
                    if status == "Post has been removed."{
                        self.getAllPosts()
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }

            }
        }
    }
    
    
    
    
    func getAllPosts(){
        if allPosts.count == 0{
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "page" : 1,
                                           "per_page":100,
                                           "filters":self.selectedFilters]
        
        
        Utility.getAllTimelinePostsFiltered(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                self.allPosts.removeAll()
                if let postsDicts = (response!["Posts"] as? [[String:Any]]){
                    
                    for postsDict in postsDicts {
                        if let postsObj = PostModel.deserialize(from: postsDict) {
                            self.allPosts.append(postsObj)
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                }
                
                self.allPosts = self.allPosts.sorted(by: { $0.creationDate! > $1.creationDate!})
                
                self.mainTableView.reloadData()
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TimelineViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as! PostTableViewCell
        let post = self.allPosts[indexPath.row]
        cell.postTitle.text = post.postTitle
        cell.postDate.text = post.creationDate.timeAgoSinceNow
        cell.postDescription.text = post.postDescription
        cell.postLikeBtn.setTitle("\(post.total_likes!)", for: .normal)
        cell.postCommentBtn.setTitle("\(post.total_comments!)", for: .normal)
        
        cell.subjectsText.text = ""
        for subject in post.subjects!{
            if cell.subjectsText.text!.count > 0 {
                cell.subjectsText.text = "\(cell.subjectsText.text!), \(subject.subjectName!)"
            }
            else{
                cell.subjectsText.text = "\(subject.subjectName!)"
            }
        }
        
        cell.postImage.image = UIImage(named:"noImage")

        var mediaUrl : String = ""
        for media in post.medias!{
            if media.mediaType == "image"{
                mediaUrl = media.mediaLink
                //mediaUrl = mediaUrl.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                break
            }
        }
        if mediaUrl.count > 0 {
            print("mediaUrl = \(mediaUrl)")

            cell.postImage.setImage(url: URL(string: mediaUrl)!, placeholder: UIImage(named:"noImage"))
            cell.postImage.setImage(url: URL(string: mediaUrl)!) { result in
                switch result {
                case .value(let image):
                    cell.postImage.image = image
                case .error(let error):
                    cell.postImage.image = UIImage(named:"noImage")
                    print("Image Load error = \(error.localizedDescription)")
                }
                cell.layoutIfNeeded()
            }
        }
        else{
            for media in post.medias!{
                if media.mediaType == "video"{
                    Utility.captureThumbnail(withVideoURL: URL(string: media.mediaLink)!, secs: 0, preferredTimeScale: 1) { (image) in
                        DispatchQueue.main.async {
                            cell.postImage.image = image
                            cell.layoutIfNeeded()
                        }
                    }
                    break
                }
            }
            
            
            
            
            
        }
        cell.postImage.contentMode = .scaleAspectFill
        cell.postImage.clipsToBounds = true

        
        cell.deleteBtn.isHidden = true
        cell.editBtn.isHidden = true

        
        if let userObj : UserModel = Utility.getUserFromPreference(){
            if userObj.type == "SuperAdmin"{
                cell.deleteBtn.isHidden = false
                cell.editBtn.isHidden = false
            }
            
            if post.creator.id == userObj.id {
                cell.deleteBtn.isHidden = false
                cell.editBtn.isHidden = false
            }
        }

        
        
        
        
        cell.deleteBtn.addTarget(self, action: #selector(deletePostAction), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        
        cell.editBtn.addTarget(self, action: #selector(editPostAction), for: .touchUpInside)
        cell.editBtn.tag = indexPath.row
        
        cell.postLikeBtn.addTarget(self, action: #selector(likePostAction), for: .touchUpInside)
        cell.postLikeBtn.tag = indexPath.row

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let post = self.allPosts[indexPath.row]
        self.performSegue(withIdentifier: "ToDetail", sender: post)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToEditPost"{
            if sender is PostModel {
                let vc = segue.destination as! EditPostViewController
                vc.selectedPost = (sender as! PostModel)
            }
        }
        else{
            if sender is PostModel {
                let vc = segue.destination as! PostDetailViewController
                vc.selectedPost = (sender as! PostModel)
            }
        }
    }
    
}
