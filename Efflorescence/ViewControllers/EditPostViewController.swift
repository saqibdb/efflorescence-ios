//
//  EditPostViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 5/3/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import DateToolsSwift
import Imaginary
import DropDown
import SwiftEntryKit

import ImagePicker
import Foundation
import MobileCoreServices
import YangMingShan
import SimpleImageViewer

class EditPostViewController: UIViewController {

    @IBOutlet weak var classNameText: UILabel!
    
    @IBOutlet weak var classFilterBtn: UIButton!
    
    @IBOutlet weak var childNameText: UILabel!
    
    @IBOutlet weak var childFilterBtn: UIButton!
    
    @IBOutlet weak var postTitleText: UITextField!
    
    @IBOutlet weak var postDescription: UITextView!
    
    @IBOutlet weak var mediaCollection: UICollectionView!
        
    var selectedMedias : [Any] = [Any]()

    var selectedPost : PostModel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        

        self.mediaCollection.delegate = self
        self.mediaCollection.dataSource = self
        
        self.mapPostWithUI()
    }
    
    
    func mapPostWithUI(){

        self.postTitleText.text = self.selectedPost.postTitle
        self.classNameText.text = "\(self.selectedPost.classId!)"
        
        if self.selectedPost.tagged_users!.count > 0 {
            var childs = self.selectedPost.tagged_users![0].userName
            for user in self.selectedPost.tagged_users!{
                if !(childs?.contains(user.userName))!{
                    childs = "\(childs!), \(user.userName!)"
                }
            }
            self.childNameText.text = childs
        }
        
        
        self.postDescription.text = self.selectedPost.postDescription
        
        if self.selectedPost.medias != nil {
            if (self.selectedPost.medias?.count)! > 0 {
                self.selectedMedias = self.selectedPost.medias!

            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    @IBAction func classFilterAction(_ sender: UIButton) {

    }
    
    @IBAction func childFilterAction(_ sender: UIButton) {

        
    }
    
    @IBAction func publishAction(_ sender: UIButton) {
        postTitleText.endEditing(true)
        postDescription.endEditing(true)
        
        
        if postTitleText.text?.count != 0 && postDescription.text?.count != 0 {
            SwiftSpinner.show("Updating...", animated: true)
            let userObj : UserModel! = Utility.getUserFromPreference()
            let auth = userObj.authToken
            
            

            
            var mediaDataArray : [[String:Any]] = [[String:Any]]()

            var checker = 0
            var gotError : Bool = false
            for media in self.selectedMedias{
                var mediaStr = ""
                var mediaType = "image"
                
                var data : Data!
                var type : Utility.DataType = .photo
                
                if media is UIImage {
                    mediaStr = (media as! UIImage).toBase64()!
                    mediaType = "image"
                    
                    data = (media as! UIImage).jpegData(compressionQuality: 0.9)
                    type = .photo
                    checker = checker + 1
                }
                
                if media is URL {
                    if let videoData = try? NSData(contentsOf: media as! URL, options: .mappedIfSafe) {
                        mediaStr = videoData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                        mediaType = "video"
                        data = videoData as Data
                        type = .video
                        checker = checker + 1
                    }
                }
                
                if media is MediaModel {
                    continue
                }
                
                Utility.uploadToAmazonS3(withData: data, andDataType: type, andResponseHandler: { (status, error, response) in
                    if status == false {
                        checker = checker - 1
                        print(error!)
                        gotError = true
                    }
                    else{
                        checker = checker - 1
                        print("Response = \(String(describing: response!["url"]!))")
                        let mediaParam : [String:Any] = ["media_link" : response!["url"]! as! String,
                                                         "media_type" : mediaType]
                        mediaDataArray.append(mediaParam)
                    }
                    
                    if checker == 0 {
                        if gotError == true {
                            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Please try again", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                            SwiftSpinner.hide()
                            return
                        }
                        
                        let parameters = [
                            "auth_token": auth!,
                            "post_id": self.selectedPost.id,
                            "PostData": [
                                "title": self.postTitleText.text!,
                                "description": self.postDescription.text!,
                                "removed_media_ids": [],
                                "new_medias": mediaDataArray
                            ]
                            ] as [String : Any]
                        Utility.updatePostWithMedia(withParameters: parameters, andResponseHandler: { (status, error, response) in
                            SwiftSpinner.hide()
                            
                            if status == false {
                                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                            }
                            else{
                                if let status = (response!["Status"] as? String){
                                    if status == "Post Updated."{
                                        for vc in (self.navigationController?.viewControllers)!{
                                            if vc is TimelineViewController{
                                                self.navigationController?.popToViewController(vc, animated: true)
                                                break
                                            }
                                        }
                                        
                                        if UIApplication.topViewController() is TimelineViewController {
                                            let topVC = UIApplication.topViewController() as! TimelineViewController
                                            topVC.allPosts.removeAll()
                                        }
                                    }
                                    else{
                                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                                    }
                                }
                            }
                        }, andProgressHandler: { (progress) in
                            SwiftSpinner.show("Posting... \(Int(progress!*100.0))%", animated: true)
                        })
                    }
                }) { (progress) in
                }
            }
            
            
            
            
           
            
            
        }
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
    
    
    func showAlertView() {
        let attributes = AlertsPopUps.bottomAlertAttributes
        
        // Generate textual content
        let title = EKProperty.LabelContent(text: "Photos", style: .init(font: MainFont.medium.with(size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "You can select Photos from Gallery or camera.", style: .init(font: MainFont.light.with(size: 13), color: .black, alignment: .center))
        let image = EKProperty.ImageContent(imageName: "imageIconLarge", size: CGSize(width: 25, height: 25), contentMode: .scaleAspectFit)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        
        // Generate buttons content
        let buttonFont = MainFont.medium.with(size: 16)
        
        // Close button
        let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Gray.a800)
        let closeButtonLabel = EKProperty.LabelContent(text: "Cancel", style: closeButtonLabelStyle)
        let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Gray.a800.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
        }
        
        // Remind me later Button
        let laterButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let laterButtonLabel = EKProperty.LabelContent(text: "Camera", style: laterButtonLabelStyle)
        let laterButton = EKProperty.ButtonContent(label: laterButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            //            let imagePickerController =
            //            imagePickerController.delegate = self
            //            self.present(imagePickerController, animated: true, completion: nil)
            let status = PHPhotoLibrary.authorizationStatus()
            
            switch status {
            case .authorized:
                let cameraVc = UIImagePickerController()
                cameraVc.sourceType = .camera
                cameraVc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                
                
                cameraVc.delegate = self
                cameraVc.allowsEditing = true
                cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium

                self.present(cameraVc, animated: true, completion: nil)
            // show your media picker
            case .denied: break
            // probably alert the user that they need to grant photo access
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized {
                        let cameraVc = UIImagePickerController()
                        cameraVc.sourceType = .camera
                        cameraVc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                        
                        
                        cameraVc.delegate = self
                        cameraVc.allowsEditing = true
                        cameraVc.videoQuality = UIImagePickerController.QualityType.typeMedium

                        self.present(cameraVc, animated: true, completion: nil)
                    }
                })
            case .restricted: break
                // probably alert the user that photo access is restricted
            }
            
            
        }
        
        // Ok Button
        let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let okButtonLabel = EKProperty.LabelContent(text: "Gallery", style: okButtonLabelStyle)
        let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            let pickerViewController = YMSPhotoPickerViewController.init()
            pickerViewController.numberOfPhotoToSelect = 100
            self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
            
        }
        
        // Generate the content
        let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, laterButton, closeButton, separatorColor: EKColor.Gray.light, expandAnimatedly: true)
        
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        
        // Setup the view itself
        let contentView = EKAlertMessageView(with: alertMessage)
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    
    @objc func removeItem(sender: UIButton!) {
        self.selectedMedias.remove(at: sender.tag)
        self.mediaCollection.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension EditPostViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedMedias.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell!
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddMediaCollectionViewCell", for: indexPath) as! AddMediaCollectionViewCell
        
        
        if indexPath.row != self.selectedMedias.count {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionViewCell", for: indexPath) as! MediaCollectionViewCell
            
            let media = self.selectedMedias[indexPath.row]
            if media is UIImage{
                (cell as! MediaCollectionViewCell).mainImage.image = (media as! UIImage)
                
            }
            if media is MediaModel {
                if (media as! MediaModel).mediaType == "image" {
                    (cell as! MediaCollectionViewCell).mainImage.setImage(url: URL(string: (media as! MediaModel).mediaLink)!, placeholder: UIImage(named:"noImage"))
                    
                }
                
                if (media as! MediaModel).mediaType == "video" {
                    Utility.captureThumbnail(withVideoURL: URL(string: (media as! MediaModel).mediaLink)!, secs: 0, preferredTimeScale: 1) { (image) in
                        DispatchQueue.main.async {
                            (cell as! MediaCollectionViewCell).mainImage.image = image
                            cell.layoutIfNeeded()
                        }
                    }
                }
            }
            if media is URL{
                Utility.captureThumbnail(withVideoURL: media as! URL, secs: 0, preferredTimeScale: 1) { (image) in
                    DispatchQueue.main.async {
                        (cell as! MediaCollectionViewCell).mainImage.image = image
                        cell.layoutIfNeeded()
                    }
                }
            }
            (cell as! MediaCollectionViewCell).mainImage.clipsToBounds = true
            (cell as! MediaCollectionViewCell).removeImage.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
            (cell as! MediaCollectionViewCell).removeImage.tag = indexPath.row
        }
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == self.selectedMedias.count {
            self.showAlertView()
        }
    }
}


// MARK: - YangMingShan Delegate
extension EditPostViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImagePickerDelegate, YMSPhotoPickerViewControllerDelegate {
    
    
    
    // MARK: - ImagePicker Delegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        guard info[.mediaType] != nil else { return }
        let mediaType = info[.mediaType] as! CFString
        
        switch mediaType {
        case kUTTypeImage:
            let image = info[.originalImage] as? UIImage
            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
            self.selectedMedias.append(resizedImage)
            self.mediaCollection.reloadData()
            break
        case kUTTypeMovie:
            let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            self.selectedMedias.append(videoURL!)
            self.mediaCollection.reloadData()
            break
        case kUTTypeLivePhoto:
            break
        default:
            break
        }
        
    }
    
    
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        dismiss(animated: true, completion: nil)
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController){
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            
            for asset: PHAsset in photoAssets
            {
                switch asset.mediaType {
                case .image:
                    imageManager.requestImageData(for: asset, options: options) { data, _, _, _ in
                        if let data = data {
                            let image = UIImage(data: data)
                            let resizedImage = Utility.imageWithImage(sourceImage: image!, scaledToWidth: 800)
                            self.selectedMedias.append(resizedImage)
                            
                        }
                    }
                case .video:
                    asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
                        
                        if ((contentEditingInput!.audiovisualAsset as? AVURLAsset)?.url) != nil {
                            PHImageManager.default().requestAVAsset(forVideo: asset, options: PHVideoRequestOptions(), resultHandler: { (assetNew, audioMix, info) -> Void in
                                if let assetNewOne = assetNew as? AVURLAsset {
                                    
                                    let videoURL = assetNewOne.url
                                    self.selectedMedias.append(videoURL)
                                    DispatchQueue.main.async {
                                        self.mediaCollection.reloadData()
                                    }
                                }
                            })
                        }
                    })
                case .audio:
                    print("Audio")
                default:
                    print("Unknown")
                }
                
                
                
                self.mediaCollection.reloadData()
                
            }
            // Assign to Array with images
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        print("Imaged...")
    }
}


