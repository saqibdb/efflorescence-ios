//
//  AllStudentsViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 26/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper


protocol StudentsSelectionnDelegate:class {
    func studentsSelectionnCompleted(selectedStudents: [Students])
}



class AllStudentsViewController: UIViewController {
     weak var delegate: StudentsSelectionnDelegate?
    
 @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var allStudents = [Students]()
    var selectedStudents =  [Students]()
     var initialValue = 1
    var allStudentsResponse: AllStudentsResponseData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        self.mainCollectionView.allowsMultipleSelection = true
        allStudents(page: initialValue)

        
    }
    
    @IBAction func addStudentsBtnTap(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.studentsSelectionnCompleted(selectedStudents: self.selectedStudents)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func allStudents(page: Int)  {
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth, "page" : page, "per_page" : 30]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allStudents)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
           
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllStudentsResponseData>().map(JSONString: json){
                        self.allStudentsResponse = dataObject
                        for i in 0..<self.allStudentsResponse!.students!.count {
                            self.allStudents.append(self.allStudentsResponse!.students![i])
                        }
                        if self.allStudentsResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allStudents(page: self.initialValue)
                            
                        }
                        else {
                            
                            
                            self.mainCollectionView.reloadData()
                        }
                        
                        
                        
                        
                        
                    }
                }
            }
        }
    }


}
extension AllStudentsViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allStudents.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentCollectionViewCell", for: indexPath) as! StudentCollectionViewCell
        cell.selectAllView.isHidden = false
        if indexPath.row > 0{
            cell.selectAllView.isHidden = true
            let student = self.allStudents[indexPath.row-1]
            //cell.studentName.text = student.userName
            
            cell.studentProfilePictureView.setImage(url: URL(string: student.profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
            cell.studentProfilePictureView.setImage(url: URL(string: student.profilePictureURL!)!) { result in
                switch result {
                case .value(let image):
                    cell.studentProfilePictureView.image = image
                case .error(let error):
                    cell.studentProfilePictureView.image = UIImage(named:"noImage")
                    print("Image Load error = \(error.localizedDescription)")
                }
                cell.layoutIfNeeded()
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            for row in 0..<collectionView.numberOfItems(inSection: 0) {
                collectionView.deselectItem(at: IndexPath(row: row, section: 0), animated: false)
            }
            self.selectedStudents.removeAll()
        }
        else{
            let student = self.allStudents[indexPath.row-1]
            self.selectedStudents = self.selectedStudents.filter{ $0.id != student.id }
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.deselectItem(at: indexPath, animated: true)
        let cell =  collectionView.cellForItem(at: indexPath) as! StudentCollectionViewCell
        if indexPath.row == 0 {
            if cell.isSelected == true {
                for row in 0..<collectionView.numberOfItems(inSection: 0) {
                    collectionView.selectItem(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: [])
                }
                self.selectedStudents = self.allStudents
            }
            
            
        }
        else{
            let student = self.allStudents[indexPath.row-1]
            if cell.isSelected == true {
                self.selectedStudents.append(student)
            }
        }
        
    }
}
