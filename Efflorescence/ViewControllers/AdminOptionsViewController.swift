//
//  AdminOptionsViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import FAPanels

class AdminOptionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
    

    @IBOutlet weak var adminOptionsTblView: UITableView!
    var optionImages: [UIImage] = [#imageLiteral(resourceName: "viewSchool"),#imageLiteral(resourceName: "viewBranches"),#imageLiteral(resourceName: "viewClasses"),#imageLiteral(resourceName: "allTeacher"),#imageLiteral(resourceName: "allStudent"),#imageLiteral(resourceName: "allStudent"),#imageLiteral(resourceName: "allStudent")]
    var optionNames: [String] = ["View All Schools","View All Branches","View All Classes","View All Teachers","View All Admins","View All Subjects","View All Parents"]
    var controllers: [String] = ["AllSchoolsViewController","AllBranchesViewController","AllClassesViewController","AllTeachersViewController","AllAdminsViewController","AllSubjectsViewController","AllParentsViewController"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adminOptionsTblView.delegate = self
        adminOptionsTblView.dataSource = self
        

       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = adminOptionsTblView.dequeueReusableCell(withIdentifier: "cell") as! AdminOptionsTableViewCell
        cell.optionsImageView.image = optionImages[indexPath.row]
        cell.optionsLabel.text = optionNames[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: controllers[indexPath.row]) 
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
}
