//
//  AddNewSchoolVC.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 23/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftSpinner

class AddNewSchoolVC: UIViewController {
    
    @IBOutlet weak var schoolNameTF: UITextField!
    @IBOutlet weak var schoolAddressTF: UITextField!

    @IBOutlet weak var headerLbl: UILabel!
    
    var edit = false
     var selectedSchool : Schools!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if edit == true {
            headerLbl.text = "Edit School"
            schoolNameTF.text = selectedSchool.schoolName
            schoolAddressTF.text = selectedSchool.schoolAddress 
        }
    }
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAndAddAnotherTap(_ sender: Any) {
        
        if schoolAddressTF.text == "" || schoolNameTF.text == "" {
            let alert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else {
            if edit == true {
                if self.selectedSchool == nil {
                    AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
                    return;
                }
                
                
                editSchool(school_id: selectedSchool.id!, school_address: schoolAddressTF.text!, school_name: schoolNameTF.text!)
            }
            else {
                 addNewSchool(school_name: schoolNameTF.text!, school_address: schoolAddressTF.text!)
            }
        }
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    
    
    func addNewSchool(school_name: String, school_address: String)  {
 
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "school_name" : school_name, "school_address":school_address]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addSchool)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error == "There is already a school present with this name and address" {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                            self.schoolNameTF.text = ""
                            self.schoolNameTF.placeholder = "School Name"
                            self.schoolAddressTF.text = ""
                            self.schoolAddressTF.placeholder = "School Address"
                            
                            let alert = UIAlertController(title: "Add", message: "New School Added", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            
                            //self.present(alert, animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                        } 
                    }
                }
            }
        }
    }
    
    func editSchool(school_id: Int, school_address: String, school_name: String)  {

        
        SwiftSpinner.show("Saving...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        
        
        let params: Parameters = ["auth_token" : auth!, "school_id" : school_id, "SchoolData":["school_name":school_name,"school_address":school_address]]
        
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.editScool)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error == "There is already a school present with this name and address" {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                          _ = self.navigationController?.popViewController(animated: true)
                            
                        }
                    }
                }
            }
        }
    }
}
