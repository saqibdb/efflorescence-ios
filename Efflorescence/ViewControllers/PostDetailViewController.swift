//
//  PostDetailViewController.swift
//  Efflorescence
//
//  Created by ibuildx Macbook 1 on 4/25/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import HandyJSON
import DateToolsSwift
import Lightbox
import Imaginary


class PostDetailViewController: UIViewController {
    var selectedPost : PostModel!
    
    var postID : Int!

    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var postTile: UILabel!
    
    @IBOutlet weak var postDate: UILabel!
    
    @IBOutlet weak var postDescription: UILabel!
    
    
    @IBOutlet weak var selectedMediaView: UIView!
    
    @IBOutlet weak var mediasCollection: UICollectionView!
    
    @IBOutlet weak var selectedMediaImage: UIImageView!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    
    @IBOutlet weak var commentBtn: UIButton!
    
    
    @IBOutlet weak var commentTableView: UITableView!
    var refreshControl = UIRefreshControl()

    
    @IBOutlet weak var commentTextView: UITextView!
    
    
    
    @IBOutlet weak var postDescriptionHeightConstraint: NSLayoutConstraint!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentTableView.delegate = self
        self.commentTableView.dataSource = self
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        
        self.commentTableView.addSubview(refreshControl) // not required when using UITableViewController
        
        self.mediasCollection.delegate = self
        self.mediasCollection.dataSource = self
        
        
        
    }
    
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        
        SwiftSpinner.show("Fetching...")
        getDetailsOfPost()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.refreshControl.endRefreshing()
    }
    
    @objc func imageTapped(gesture: UIGestureRecognizer) {
       
        // Create an array of images.
        var images = [LightboxImage]()
        
        var currentPage = 0
        
        let medias : Array<MediaModel> = (self.selectedPost.medias)!
        for media in medias{
            if media.mediaType == "image"{
                let lightBoxImage = LightboxImage(imageURL: URL(string: media.mediaLink)!)
                images.append(lightBoxImage)
            }
            if media.mediaType == "video"{
                let lightBoxImage = LightboxImage(
                    image: UIImage(named: "noImage")!,
                    text: "",
                    videoURL: URL(string: media.mediaLink)
                )
                images.append(lightBoxImage)
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        controller.modalPresentationStyle = .fullScreen

        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        
        present(controller, animated: true, completion: nil)
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.selectedPost != nil {
            mapPostWithUI()
        }
        SwiftSpinner.show("Loading...")
        getDetailsOfPost()
    }
    
    func mapPostWithUI(){
        self.pageTitle.text = self.selectedPost.postTitle
        self.postTile.text = self.selectedPost.postTitle
        self.postDate.text = self.selectedPost.creationDate.timeAgoSinceNow
        self.postDescription.text = self.selectedPost.postDescription
        
        self.view.layoutIfNeeded()
        
        print("self.postDescription = \(self.postDescription.frame.size.height)")
        self.postDescriptionHeightConstraint.constant = self.postDescription.frame.size.height + 50.0
        self.view.layoutIfNeeded()

        
        
        self.likeBtn.setTitle("\(self.selectedPost.total_likes!)", for: .normal)
        self.commentBtn.setTitle("\(self.selectedPost.total_comments!)", for: .normal)
        self.commentTableView.reloadData()
        
        if self.selectedPost.medias != nil {
            if (self.selectedPost.medias?.count)! > 0 {
                
                
                let medias : Array<MediaModel> = (self.selectedPost.medias)!
                let media = medias[0]
                self.selectedMediaImage.image = UIImage(named:"noImage")
                self.selectedMediaImage.contentMode = .scaleAspectFill
                self.selectedMediaImage.clipsToBounds = true
                self.selectedMediaImage.setImage(url: URL(string: media.mediaLink)!, placeholder: UIImage(named:"noImage"))
                
                // Do any additional setup after loading the view.
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(gesture:)))
                self.selectedMediaImage.addGestureRecognizer(tapGesture)
                self.selectedMediaImage.isUserInteractionEnabled = true
                
                
            }
        }
        
    }
    
    
    
    
    public func getDetailsOfPost(){
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        
        var postId = 0
        if self.selectedPost != nil {
            postId = self.selectedPost.id
        }
        if self.postID != nil {
            postId = self.postID
        }
        
        
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_id" : postId]
        Utility.getDetailsOfPost(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()

            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let postsDict = (response!["Post"] as? [String:Any]){
                    if let postsObj = PostModel.deserialize(from: postsDict) {
                        self.selectedPost = postsObj
                    }
                    else{
                        print("ISSUE IN DESERIALIZE")
                    }
                }
                self.mapPostWithUI()
            }
        }
    }
    
    
    
    
    
    
    @IBAction func likeAction(_ sender: UIButton) {
        SwiftSpinner.show("Updating...", animated: true)
        let userObj : UserModel! = Utility.getUserFromPreference()
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_id" : self.selectedPost.id]
        Utility.addRemoveLikeForPost(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let likesCount = (response!["total_likes"] as? Int){
                    self.selectedPost.total_likes = likesCount
                    self.likeBtn.setTitle("\(self.selectedPost.total_likes!)", for: .normal)

                }
            }
        })
    }
    
    @IBAction func postAction(_ sender: UIButton) {
        commentTextView.endEditing(true)
        if commentTextView.text?.count != 0{
            SwiftSpinner.show("Posting...", animated: true)
            let userObj : UserModel! = Utility.getUserFromPreference()
            let auth = userObj.authToken
            let parameters : [String : Any] = ["auth_token" : auth! ,
                                               "post_id" : self.selectedPost.id,
                                               "comment_title" : commentTextView.text!]
            Utility.addCommentToPost(withParameters: parameters, andResponseHandler: { (status, error, response) in
                SwiftSpinner.hide()
                
                if status == false {
                    AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                }
                else{
                    if let postsDict = (response!["NewPost"] as? [String:Any]){
                        if let postsObj = PostModel.deserialize(from: postsDict) {
                            self.selectedPost = postsObj
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                    self.mapPostWithUI()
                    self.commentTextView.text = ""
                }
            })
        }
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        let post = self.selectedPost
        let alert = UIAlertController(title: app_Name, message: "Are you sure you want to delete this Post?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.removePostFromTimeLine(withPostId: post!.id)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func editAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToEditPost", sender: self.selectedPost)
    }
    
    
    func removePostFromTimeLine(withPostId postId : Int){
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "post_id" : postId]
        SwiftSpinner.show("Removing...")
        
        Utility.removePostFromTimeline(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let status = (response!["Status"] as? String){
                    if status == "Post has been removed."{
                        self.navigationController?.popViewController(animated: true)
                        if UIApplication.topViewController() is TimelineViewController {
                            let topVC = UIApplication.topViewController() as! TimelineViewController
                            topVC.allPosts.removeAll()
                        }
                        
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
                
            }
        }
    }
   

    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func deleteCommentAction(sender: UIButton!) {
        
        let comment = self.selectedPost.comments![sender.tag]
        let alert = UIAlertController(title: app_Name, message: "Are you sure you want to delete this Comment?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.removeCommentFromTimeLine(withCommentId: comment.id)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    func removeCommentFromTimeLine(withCommentId commentId : Int){
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "comment_id" : commentId]
        SwiftSpinner.show("Removing...")
        
        Utility.removeCommentFromTimeline(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let status = (response!["Status"] as? String){
                    if status == "Comment has been removed."{
                        self.getDetailsOfPost()
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Failed!!!", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
                
            }
        }
    }
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if segue.identifier == "ToEditPost"{
            if sender is PostModel {
                let vc = segue.destination as! EditPostViewController
                vc.selectedPost = (sender as! PostModel)
            }
        }
        else{
            if sender is PostModel {
                let vc = segue.destination as! EditPostViewController
                vc.selectedPost = (sender as! PostModel)
            }
        }
        
     }
 
}

extension PostDetailViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedPost == nil {
            return 0
        }
        if self.selectedPost.comments == nil {
            return 0
        }
        return self.selectedPost.comments!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
        let comment = self.selectedPost.comments![indexPath.row]
        cell.commentCreatorImage.image = UIImage(named:"noImage")
        cell.commentCreatorImage.contentMode = .scaleAspectFill
        cell.commentCreatorImage.clipsToBounds = true
        if comment.creator.profilePictureURL.count > 0 {
            cell.commentCreatorImage.setImage(url: URL(string: comment.creator.profilePictureURL)!, placeholder: UIImage(named:"noImage"))
        }
        
        cell.creatorName.text = comment.creator.userName
        cell.commentText.text = comment.commentTitle
        cell.deleteBtn.addTarget(self, action: #selector(deleteCommentAction), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
        
    }
}

extension PostDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.selectedPost == nil {
            return 0
        }
        return (self.selectedPost.medias?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionViewCell", for: indexPath) as! MediaCollectionViewCell

        let medias : Array<MediaModel> = (self.selectedPost.medias)!
        let media = medias[indexPath.row]
        cell.mainImage.image = UIImage(named:"noImage")
        cell.mainImage.contentMode = .scaleAspectFill
        cell.mainImage.clipsToBounds = true
        if media.mediaType == "image" {
            cell.mainImage.setImage(url: URL(string: media.mediaLink)!, placeholder: UIImage(named:"noImage"))

        }
        
        if media.mediaType == "video" {
            Utility.captureThumbnail(withVideoURL: URL(string: media.mediaLink)!, secs: 0, preferredTimeScale: 1) { (image) in
                DispatchQueue.main.async {
                    cell.mainImage.image = image
                    cell.layoutIfNeeded()
                }
            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let medias : Array<MediaModel> = (self.selectedPost.medias)!
        let media = medias[indexPath.row]
        self.selectedMediaImage.image = UIImage(named:"noImage")
        self.selectedMediaImage.contentMode = .scaleAspectFill
        self.selectedMediaImage.clipsToBounds = true
        self.selectedMediaImage.setImage(url: URL(string: media.mediaLink)!, placeholder: UIImage(named:"noImage"))
    }
}
extension PostDetailViewController: LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}
