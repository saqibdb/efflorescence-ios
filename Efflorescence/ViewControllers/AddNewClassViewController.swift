//
//  AddNewClassViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 26/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import DropDown
import SwiftSpinner
import ObjectMapper
import Alamofire
import TagListView

class AddNewClassViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource {
  
    
    @IBOutlet var dropImageView: [UIImageView]!
    @IBOutlet weak var firstRowHeight: NSLayoutConstraint!
    @IBOutlet weak var secondRowHeight: NSLayoutConstraint!
    @IBOutlet weak var savedButton: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var classNameTF: UITextField!
    @IBOutlet weak var branchBtn: UIButton!
    @IBOutlet weak var branchTF: UITextField!

    @IBOutlet weak var schoolFilterBtn: UIButton!

    @IBOutlet weak var selectedSchoolTF: UITextField!
    
    
    let classesDropDown = DropDown()
    let branchDropDown = DropDown()
    var selectedStudent = [Students]()
    var cellCount = 1
    var schoolsResponse: AllSchoolResponse!
    var totalResponse = [Schools]()
    var totalSubjects = [Subjectss]()

    var initialValue = 1
    var subjectInitialValue = 1
    var selectedSchool : Schools!
    var selectedSchoolID: Int!
    var branchesResponse: AllBranchesResponse!
    var subjectResponse: AllSubjectsResponseData!
    var selectedBranch : BranchModel!
    var selectedBranchId : Int!
    var selectedStudentArr = [Int]()
    var selectedSubjectArr = [Int]()
    var edit = 0
    var classPassResponse: Classes!
    
    var allBranches : [BranchModel] = [BranchModel]()
    var allSubjects : [SubjectModel] = [SubjectModel]()
    var selectedSubjects : [SubjectModel] = [SubjectModel]()

    @IBOutlet weak var selectedSubjectsTagsView: TagListView!
    let subjetcsDropDown = DropDown()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.edit == 1 {
            self.headerLbl.text = "Edit Class"
            self.savedButton.setTitle("Save", for: .normal)
            firstRowHeight.constant = 0
            secondRowHeight.constant = 0
            dropImageView[0].isHidden = true
            dropImageView[1].isHidden = true
            classNameTF.text = classPassResponse.className
            
            
            
            self.selectedSubjectsTagsView.removeAllTags()

            if classPassResponse.subjects!.count > 0 {
                for subject in classPassResponse.subjects!{
                    self.selectedSubjectsTagsView.addTag(subject.subjectName!)
                }
            }
            
            
        }

        branchDropDown.anchorView = branchBtn
        branchDropDown.dataSource = []
        branchDropDown.width = 200
        branchDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.branchDropDown.hide()
            self.branchTF.text = item
            self.selectedBranch = self.allBranches[index]
            self.selectedBranchId = self.selectedBranch.id!
            
        }
        
        classesDropDown.anchorView = schoolFilterBtn
        classesDropDown.dataSource = []
        classesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.classesDropDown.hide()
            self.selectedSchoolTF.text = item
            self.selectedSchool = self.totalResponse[index]
            self.selectedSchoolID = self.selectedSchool.id!
            self.allSchoolBranchAPI()
        }
        classesDropDown.width = 200
        
        // The view to which the drop down will appear on
        subjetcsDropDown.anchorView = selectedSubjectsTagsView    // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        subjetcsDropDown.dataSource = []
        
        
        // Action triggered on selection
        subjetcsDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.subjetcsDropDown.hide()
            
            let selectedSubject = self.allSubjects[index]
            if self.selectedSubjects.contains(where: { subject in subject.id == selectedSubject.id }) {
                print("1 exists in the array")
            } else {
                self.selectedSubjectsTagsView.addTag(item)
                self.selectedSubjects.append(selectedSubject)
            }
            
        }
        // Will set a custom width instead of the anchor view width
        subjetcsDropDown.width = 200
        self.selectedSubjectsTagsView.delegate = self
       
        
        allSchoolsAPI(page: initialValue)
        allSubjectsAPI(page: subjectInitialValue)
        getSubjects()
       
    }
    @IBAction func classFilterAction(_ sender: UIButton) {
        classesDropDown.show()
    }
    
    @IBAction func branchBtnTap(_ sender: Any) {
        branchDropDown.show()
    }
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func subjectFilterAction(_ sender: UIButton) {
        subjetcsDropDown.show()
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedStudent.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    
       let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddMediaCollectionViewCell", for: indexPath) as! AddMediaCollectionViewCell
        
        
        if indexPath.row != self.selectedStudent.count {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionViewCell", for: indexPath) as! MediaCollectionViewCell
            
           
            cell.mainImage.setImage(url: URL(string: selectedStudent[indexPath.row].profilePictureURL!)!, placeholder: UIImage(named:"noImage"))
         
            
                cell.mainImage.clipsToBounds = true
                cell.removeImage.addTarget(self, action: #selector(removeItem), for: .touchUpInside)
                cell.removeImage.tag = indexPath.row
           
            return cell
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == self.selectedStudent.count {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AllStudentsViewController") as? AllStudentsViewController
            vc?.delegate = self
            
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }
    @objc func removeItem(sender: UIButton!) {
        self.selectedStudent.remove(at: sender.tag)

    }
    
    @IBAction func saveBtnTap(_ sender: Any) {
        var studentIds: [Int] = [Int]()
        
        
        for subject in self.selectedSubjects {
            studentIds.append(subject.id)
        }
        if self.edit == 1 {
            for subject in self.classPassResponse.subjects! {
                studentIds.append(subject.id!)
            }
            if self.classPassResponse == nil || self.classNameTF.text?.count == 0 {
                AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
                return;
            }
            
            
            editClass(class_id: classPassResponse.id!, class_name: classNameTF.text!, subject_id: studentIds, student_id: selectedStudentArr)
        }
        else {
            
            if self.selectedBranchId == nil || self.classNameTF.text?.count == 0 {
                AlertsPopUps.showPopupMessage(title: "Incomplete", titleColor: .white, description: "Please Fill All Details.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .warning)
                return;
            }
            
            
            addNewClass(class_name: classNameTF.text!, branch_id: selectedBranchId, subject_id: studentIds, student_id: selectedStudentArr)
        }
        
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func allSchoolBranchAPI()  {
        SwiftSpinner.show("Getting Branches...", animated: true)
        if selectedSchoolID == nil {
            return
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: [String : Any] = ["auth_token" : auth!, "school_id" : self.selectedSchoolID]
        
        
        Utility.getBranchesOfSchool(withParameters: params) { (status, error, response) in
            SwiftSpinner.hide()
            

            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let schoolDict = (response!["School"] as? [String:Any]) {
                    if let branchesDicts = (schoolDict["branches"] as? [[String:Any]]) {
                        self.allBranches.removeAll()
                        self.branchDropDown.dataSource.removeAll()
                        for branchesDict in branchesDicts {
                            if let branchObj = BranchModel.deserialize(from: branchesDict) {
                                self.allBranches.append(branchObj)
                                self.branchDropDown.dataSource.append(branchObj.branchName)
                            }
                            else{
                                print("ISSUE IN DESERIALIZE")
                            }
                        }
                    }
                }
            }
        }
    }
    func allSubjectsAPI(page: Int)  {
        //  SwiftSpinner.show("Fetching...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : page, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSubjects)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        SwiftSpinner.hide()
        
        Alamofire.request(request).responseJSON { response in
            
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSubjectsResponseData>().map(JSONString: json){
                        self.subjectResponse = dataObject
                        
                        
                        for i in 0..<self.subjectResponse!.subjects!.count {
                            self.totalSubjects.append(self.subjectResponse!.subjects![i])
                        }
                        
                        if self.subjectResponse.nextPage == true {
                            self.subjectInitialValue = self.subjectInitialValue + 1
                            self.allSubjectsAPI(page: self.subjectInitialValue)
                            
                        }
                        else {
                            
                            SwiftSpinner.hide()
                            
                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    
    func editClass(class_id: Int, class_name: String,subject_id: [Int],student_id: [Int])  {
        SwiftSpinner.show("Saving...")
        
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        var subjectID : [[String:Any]] = [[String:Any]]()
        var studentID : [[String:Any]] = [[String:Any]]()
        for i in 0..<subject_id.count {
            let tagParam : [String:Any] = ["subject_id": subject_id[i]]
            subjectID.append(tagParam)
        }
        for i in 0..<student_id.count {
            let tagParam : [String:Any] = ["student_id": student_id[i]]
            studentID.append(tagParam)
        }
        
        let params: Parameters = ["auth_token": auth!,"class_id": class_id,"ClassData":["class_name":class_name,"class_subjects":subjectID,"students":studentID]]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.editClass)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error != nil {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                            
                            let alert = UIAlertController(title: "Add", message: "Class Update Successfully", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            
                            //self.present(alert, animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)

                        }
                        
                        
                        
                        
                    }
                }
            }
        }
    }
    
    func addNewClass(class_name: String, branch_id: Int,subject_id: [Int],student_id: [Int])  {
        SwiftSpinner.show("Saving...")
        
        
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        var subjectID : [[String:Any]] = [[String:Any]]()
        var studentID : [[String:Any]] = [[String:Any]]()
        for i in 0..<subject_id.count {
            let tagParam : [String:Any] = ["subject_id": subject_id[i]]
            subjectID.append(tagParam)
        }
        for i in 0..<student_id.count {
            let tagParam : [String:Any] = ["student_id": student_id[i]]
            studentID.append(tagParam)
        }
        
        let params: Parameters = ["auth_token": auth!,"class_name":class_name,"branch_id":branch_id,"class_subjects":subjectID,"students": studentID]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addClassWithSubjectIds)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.error != nil {
                            let alert = UIAlertController(title: "Error", message: dataObject.error, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else {
                           
                            let alert = UIAlertController(title: "Add", message: "New Class Added", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            
                            //self.present(alert, animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)

                        }
                    }
                }
            }
        }
    }
    func allSchoolsAPI(page: Int)  {
     
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : page, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchools)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
           
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        self.schoolsResponse = dataObject
                        for i in 0..<self.schoolsResponse!.schools!.count {
                            self.totalResponse.append(self.schoolsResponse!.schools![i])
                        }
                        if self.schoolsResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allSchoolsAPI(page: self.initialValue)
                            
                        }
                        else {
                            for i in 0..<self.totalResponse.count{
                                self.classesDropDown.dataSource.append(self.totalResponse[i].schoolName!)
                            }
                        }
                    }
                }
            }
        }
    }
    

  func getSubjects(){
      if allSubjects.count == 0{
          SwiftSpinner.show("Fetching...")
      }
      let userObj : UserModel = Utility.getUserFromPreference()!
      let auth = userObj.authToken
      let parameters : [String : Any] = ["auth_token" : auth!]
      
      
      Utility.getSubjects(withParameters: parameters) { (status, error, response) in
          SwiftSpinner.hide()
          
          if status == false {
              AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
          }
          else{
              if let subjectsDicts = (response!["Subjects"] as? [[String:Any]]){
                self.allSubjects.removeAll()
                self.subjetcsDropDown.dataSource.removeAll()
                
                  for subjectDict in subjectsDicts {
                      if let subjectObj = SubjectModel.deserialize(from: subjectDict) {
                          self.allSubjects.append(subjectObj)
                          self.subjetcsDropDown.dataSource.append(subjectObj.subjectName)
                      }
                      else{
                          print("ISSUE IN DESERIALIZE")
                      }
                  }
              }
          }
      }
  }

}
extension AddNewClassViewController : StudentsSelectionnDelegate {
    
    
    func studentsSelectionnCompleted(selectedStudents: [Students]) {
        
       
        self.selectedStudent = selectedStudents
        for i in 0..<selectedStudent.count{
            selectedStudentArr.append(selectedStudent[i].id!)
        }

        //        self.childNameText.text = "\(self.selectedStudent.count) students selected."
//        self.selectedStudentsTagsView.removeAllTags()
        
//        for student in self.selectedStudent {
//            self.selectedStudentsTagsView.addTag(student.userName)
//        }
    }
    
}

extension AddNewClassViewController : TagListViewDelegate {
    @objc func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void{
        self.selectedSubjects = self.selectedSubjects.filter{ $0.subjectName != title }
        sender.removeTag(title)
    }
}
