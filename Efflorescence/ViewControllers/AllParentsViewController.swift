//
//  AllParentsViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import Alamofire
import ObjectMapper

class AllParentsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var allParentsTblView: UITableView!
    
    
    
    var allParnts : [UserModel] = [UserModel]()
    var refreshControl = UIRefreshControl()

    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.allParentsTblView.addSubview(refreshControl) // not required when using UITableViewController)
        
        
    }
  
  override func viewDidAppear(_ animated: Bool) {
      self.getAllParents()
  }
  
  @objc func refresh(sender:AnyObject) {
      self.refreshControl.endRefreshing()
      SwiftSpinner.show("Fetching...")
      self.getAllParents()
  }
  
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allParnts.count
    }
    func removeParentsAPI(parent_id: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "parent_id" : parent_id]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeParent)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllTeachersResponseModel>().map(JSONString: json){
                        self.allParnts.removeAll()
                        self.getAllParents()
                    }
                }
            }
        }
    }
    @IBAction func addNewParent(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewParentViewController") as? AddNewParentViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AllParentsTableViewCell
        let parent = self.allParnts[indexPath.row]
        
        cell.userNameTF.text = parent.userName
        cell.emailTF.text = parent.email
        if parent.branch != nil {
            cell.branchTF.text = parent.branch.branchName
            cell.schoolNameTf.text = parent.branch.school.schoolName
        }
        else{
            cell.branchTF.text = ""
            cell.schoolNameTf.text = ""
        }
        cell.phoneTF.text = parent.phone
        cell.genderTF.text = parent.gender
        cell.childs = parent.childs!
        cell.childTableView.reloadData()
            cell.deleteBtn = {
                self.removeParentsAPI(parent_id: (parent.id)!)
            }
            
            return cell
    
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   func getAllParents()  {
       if allParnts.count == 0 {
           SwiftSpinner.show("Fetching...")
       }
       let userObj : UserModel = Utility.getUserFromPreference()!
       let auth = userObj.authToken
       let params: [String : Any] = ["auth_token" : auth!, "page" : -1, "per_page" : 100]
       
       
       Utility.getParents(withParameters: params) { (status, error, response) in
           SwiftSpinner.hide()
           
           
           if status == false {
               AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
           }
           else{
               if let adminsDicts = (response!["Parents"] as? [[String:Any]]) {
                    self.allParnts.removeAll()
                   for adminsDict in adminsDicts {
                       if let adminObj = UserModel.deserialize(from: adminsDict) {
                           self.allParnts.append(adminObj)
                       }
                       else{
                           print("ISSUE IN DESERIALIZE")
                       }
                   }
                   self.allParentsTblView.reloadData()
               }
           }
       }
   }
}

