//
//  MenuViewController.swift
//  Kei Snap
//
//  Created by ibuildx Macbook 1 on 3/25/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import FAPanels
import UserNotifications
import SwiftSpinner

class MenuViewController: UIViewController {
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    
    
    @IBOutlet weak var adminOptionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var userNameBtn: UIButton!
    
    @IBOutlet weak var typeBtn: UIButton!
    
    
    @IBOutlet weak var notificationBtn: UISwitch!
 
    

    override func viewDidLoad() {
        super.viewDidLoad()

        panel?.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
        let userObj : UserModel = Utility.getUserFromPreference()!
        if !userObj.type.contains("Admin"){
            self.adminOptionHeightConstraint.constant = 0.0
        }
        else{
            self.adminOptionHeightConstraint.constant = 40.0
        }
        self.userNameBtn.setTitle(userObj.userName, for: .normal)
        self.typeBtn.setTitle(userObj.type, for: .normal)
        self.notificationBtn.isOn = userObj.isNotification
    }

    
    
    @IBAction func logoutAction(_ sender: UIButton) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"ffflorescenceLoggedUser")
        prefs.synchronize()
        UIApplication.shared.unregisterForRemoteNotifications()
        let parameters : [String : Any] = ["device_id" : UIDevice.current.identifierForVendor!.uuidString]
        Utility.unRegisterPushApns(withParameters: parameters) { (status, error, response) in
            if status == false {
                print("ERROR unRegisterPushApns= \(error!)")
            }
            else{
                print("RESPONSE unRegisterPushApns= \(response!)")
            }
        }
        panel?.closeLeft()
        let centerVC = panel?.center as! UINavigationController
        centerVC.popToRootViewController(animated: true)
    }
    
    
    @IBAction func adminOptionAction(_ sender: Any) {
        panel?.closeLeft()
        let centerVC = panel?.center as! UINavigationController
        
        //
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AdminOptionsViewController")
        centerVC.show(vc, sender: nil)
    }
    
    @IBAction func notificationChanged(_ sender: UISwitch) {
        print("\(sender.isOn)")
        SwiftSpinner.show("Updating...", animated: true)
        let userObj : UserModel! = Utility.getUserFromPreference()
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth! ,
                                           "is_notification" : sender.isOn]
        Utility.enableDisableNotifications(withParameters: parameters, andResponseHandler: { (status, error, response) in
            SwiftSpinner.hide()
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

} 

extension MenuViewController : FAPanelStateDelegate {
    func leftPanelWillBecomeActive(){
        let userObj : UserModel = Utility.getUserFromPreference()!
        
        if !userObj.type.contains("Admin"){
            self.adminOptionHeightConstraint.constant = 0.0
        }
        else{
            self.adminOptionHeightConstraint.constant = 40.0
        }
        
        self.userNameBtn.setTitle(userObj.userName, for: .normal)
        self.typeBtn.setTitle(userObj.type, for: .normal)
        self.notificationBtn.isOn = userObj.isNotification
        print("HAAHHHA")

    }
}
