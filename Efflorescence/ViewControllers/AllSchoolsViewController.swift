//
//  AllSchoolsViewController.swift
//  Efflorescence
//
//  Created by Vivek Dogra on 18/05/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftSpinner

class AllSchoolsViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {


    @IBOutlet weak var allSchoolsTableView: UITableView!
    
    @IBOutlet weak var addNewSchoolBtn: UIButton!
    
    
    
    
    
    
    var refreshControl = UIRefreshControl()

    
    
    var schoolsResponse: AllSchoolResponse!
    var totalResponse = [Schools]()
    var initialValue = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.allSchoolsTableView.delegate = self
        self.allSchoolsTableView.dataSource = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)

        
        self.allSchoolsTableView.addSubview(refreshControl) // not required when using UITableViewController
        
        let userObj : UserModel = Utility.getUserFromPreference()!

        if userObj.type == "SchoolAdmin"{
            self.addNewSchoolBtn.isHidden = true
        }
        if userObj.type == "BranchAdmin"{
            self.addNewSchoolBtn.isHidden = true
        }
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        allSchoolsAPI(page: initialValue)
    }
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        initialValue = 1
        SwiftSpinner.show("Fetching...")
        allSchoolsAPI(page: initialValue)

    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if schoolsResponse != nil {
             return totalResponse.count
        }
        else {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allSchoolsTableView.dequeueReusableCell(withIdentifier: "cell") as! AllSchoolsTableViewCell
        let userObj : UserModel = Utility.getUserFromPreference()!
        if userObj.type == "SchoolAdmin"{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        if userObj.type == "BranchAdmin"{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        
        
        
        
        
        
        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewSchoolVC") as? AddNewSchoolVC
            vc?.edit = true
            vc?.selectedSchool = self.totalResponse[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        }
        cell.deleteBtn = {
            // create the alert
            let alert = UIAlertController(title: "Alert!", message: "Do you want to remove this School", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
                
                
                self.deleteSchool(schoolId: self.totalResponse[indexPath.row].id!)
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
           
        }
        
        cell.schoolName.text = totalResponse[indexPath.row].schoolName
        cell.schoolAdress.text = totalResponse[indexPath.row].schoolAddress
        return cell
    }
    func allSchoolsAPI(page: Int)  {
        if totalResponse.count == 0 {
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "page" : -1, "per_page" : 20]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchools)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        //SwiftSpinner.hide()
        Alamofire.request(request).responseJSON { response in
            DispatchQueue.main.async {
                SwiftSpinner.hide()
            }
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        self.totalResponse.removeAll()
                        self.schoolsResponse = dataObject
                        for i in 0..<self.schoolsResponse!.schools!.count {
                            self.totalResponse.append(self.schoolsResponse!.schools![i])
                        }
                        if self.schoolsResponse.nextPage == true {
                            self.initialValue = self.initialValue + 1
                            self.allSchoolsAPI(page: self.initialValue)
                            self.allSchoolsTableView.reloadData()
                        }
                        else {
                            
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                                self.allSchoolsTableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func deleteSchool(schoolId: Int)  {
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let params: Parameters = ["auth_token" : auth!, "school_id" : schoolId]
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeSchool)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.httpBody = postData
        
        Alamofire.request(request).responseJSON { response in
            SwiftSpinner.hide()
            if let data = response.data {
                if  let json = String(data: data, encoding: String.Encoding.utf8){
                    if let dataObject = Mapper<AllSchoolResponse>().map(JSONString: json){
                        if dataObject.status == "School has been removed." {
                            self.initialValue = 1
                            DispatchQueue.main.async {
                                self.allSchoolsAPI(page: self.initialValue)
                                //SwiftSpinner.show("Fetching...")
                            }
                            print("removed")

                        }
                    }
                }
            }
        }
    }
    
    @IBAction func addSchoolTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewSchoolVC") as? AddNewSchoolVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }

}
