//
//  AllSubjectsViewController.swift
//  Efflorescence
//
//  Created by MacBook Pro on 01/10/2019.
//  Copyright © 2019 iBuildX. All rights reserved.
//



import UIKit
import Alamofire
import ObjectMapper
import SwiftSpinner

class AllSubjectsViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {


    @IBOutlet weak var allSchoolsTableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var allSubjects : [SubjectModel] = [SubjectModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.allSchoolsTableView.delegate = self
        self.allSchoolsTableView.dataSource = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)

        
        self.allSchoolsTableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getSubjects()
    }
    
    @objc func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
        
        SwiftSpinner.show("Fetching...")
        getSubjects()

    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("self.allSubjects COUNT \(self.allSubjects.count)")

        return self.allSubjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allSchoolsTableView.dequeueReusableCell(withIdentifier: "cell") as! AllSchoolsTableViewCell
        let subject = self.allSubjects[indexPath.row]

        cell.editBtn = {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewSubjectViewController") as? AddNewSubjectViewController
            vc?.selectedSubject = subject

            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        }
        cell.deleteBtn = {
            let alert = UIAlertController(title: "Alert!", message: "Do you want to remove this subject", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
                self.deleteSubject(withSubject: subject)
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        cell.schoolName.isUserInteractionEnabled = false
        cell.schoolName.text = subject.subjectName
        return cell
    }
    func getSubjects(){
        if allSubjects.count == 0{
            SwiftSpinner.show("Fetching...")
        }
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth!]
        
        
        Utility.getSubjects(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
          
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let subjectsDicts = (response!["Subjects"] as? [[String:Any]]){
                    self.allSubjects.removeAll()
                    for subjectDict in subjectsDicts {
                        if let subjectObj = SubjectModel.deserialize(from: subjectDict) {
                            self.allSubjects.append(subjectObj)
                            
                        }
                        else{
                            print("ISSUE IN DESERIALIZE")
                        }
                    }
                    DispatchQueue.main.async {
                        self.allSchoolsTableView.reloadData()
                    }
                }
            }
        }
    }
    
    
    func deleteSubject(withSubject subject : SubjectModel){
        SwiftSpinner.show("Deleting...")
        let userObj : UserModel = Utility.getUserFromPreference()!
        let auth = userObj.authToken
        let parameters : [String : Any] = ["auth_token" : auth!,
            "subject_id" : subject.id!]
        
        
        Utility.removeSubject(withParameters: parameters) { (status, error, response) in
            SwiftSpinner.hide()
            
            
            
            if status == false {
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            }
            else{
                if let status = (response!["Status"] as? String){
                    if status == "Subject has been removed."{
                        self.allSubjects.removeAll()
                        self.getSubjects()
                    }
                    else{
                        AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: status, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func addSchoolTap(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewSubjectViewController") as? AddNewSubjectViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        panel?.openLeft(animated: true)
    }

}







