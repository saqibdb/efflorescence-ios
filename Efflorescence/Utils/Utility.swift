//
//  Utility.swift
//  uChoose
//
//  Created by iBuildX on 09/07/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3
import AVFoundation


class Utility: NSObject {
    typealias ResponseHandler = (_ success:Bool, _ error : String? , _ response : [String : Any]?) -> Void
    typealias ProgressHandler = (_ progressValue : Double?) -> Void

    
    static let S3BucketName = "uchoose"
    static let accessKey = "AKIAJBOEQASD4L7YUKFQ"
    static let secretKey = "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"
    
    
    public enum DataType {
        case photo
        case video
        var value: Int? {
            switch self {
            case .photo:
                return 1
            case .video:
                return 2
            }
        }
        
//        var isValid: Bool {
//            return self != .none
//        }    //vivekdogra
    }

    
    class func login(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        print("BASE URL = \(Constant.baseUrl)")
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.login)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        if JSON["status"] as? String != "User Found" {
                            responseHandler(false , JSON["status"] as? String , nil)
                        }
                        else{
                            responseHandler(true , nil , JSON)
                        }
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    class func setPassword(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.setPassword)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    class func setPin(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.setPin)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func loginWithPin(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.loginWithPin)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        if JSON["status"] as? String != "User Found" {
                            responseHandler(false , JSON["status"] as? String , nil)
                        }
                        else{
                            responseHandler(true , nil , JSON)
                        }
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    

    class func getAllTimelinePostsFiltered(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getAllTimelinePostsFiltered)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    class func getDetailsOfPost(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getDetailsOfPost)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func addCommentToPost(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addCommentToPost)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func getTeacherClasses(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getTeacherClasses)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func getStudentOfClass(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getStudentOfClass)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func addPost(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addPost)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func addPostWithMedia(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.addPostWithMedia)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    
    class func getChildOfParent(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getChildOfParent)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func removePostFromTimeline(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removePostFromTimeline)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func updatePostWithMedia(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.updatePostWithMedia)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    class func removeCommentFromTimeline(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.removeCommentFromTimeline)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func addRemoveLikeForPost(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.addRemoveLikeForPost)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func getSubjects(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getSubjects)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func registerPushApns(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.registerDeviceApn)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func unRegisterPushApns(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.unRegisterDeviceApn)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func enableDisableNotifications(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.enableDisableNotifications)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func getAdmins(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.getAdmins)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    class func addNewTeacher(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.addTeacher)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    class func editTeacher(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.editTeacher)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    class func addNewAdmin(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.addUser)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    
    class func addNewSubject(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.addSubject)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    class func editSubject(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.editSubject)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    
    
    class func removeSubject(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.removeSubject)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    class func addParentWithChild(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let url = URL(string: "\(Constant.baseUrl)\(Constant.addParentWithChild)")!
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        Alamofire.upload(postData!, to: url, method: .post, headers: ["Content-Type": "application/json"]).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
            }.responseJSON { (response) in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        responseHandler(true , nil , JSON)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }
    }
    
    class func getParents(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allParents)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func uploadToAmazonS3(withData data : Data,andDataType dataType : DataType, andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        var mimeType =  "image/jpeg"
        var fileExtension = ".png"
        if dataType == .video {
            fileExtension = ".mp4"
            mimeType =  "video/mp4"
        }
        
        let fileName = ProcessInfo.processInfo.globallyUniqueString + fileExtension
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        do {
            try data.write(to: fileURL, options: .atomic)
        } catch {
            print(error)
        }
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = fileName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = mimeType
        uploadRequest.acl = .publicRead
        uploadRequest.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            let percentageUploaded = (Float(totalBytesSent) / Float(totalBytesExpectedToSend)) * 100
            var progressDouble = Double(percentageUploaded)
            DispatchQueue.main.async(execute: {
                progressHandler(progressDouble)
            })
        }
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread()) { (task) -> AnyObject! in
            if let error = task.error {
                responseHandler(false , error.localizedDescription , nil)
            }
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                    responseHandler(true , nil , ["url" : absoluteString as Any])
                }
            }
            
            return nil
        }
    }
    
    
    
    class func getAllSchools(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchools)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    
    
    class func getBranchesOfSchool(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allSchoolBranches)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    class func getBranchClasses(withParameters parameters : [String : Any], andResponseHandler responseHandler : @escaping ResponseHandler) {
        var request = URLRequest(url: URL(string: "\(Constant.baseUrl)\(Constant.allClasses)")!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = postData
        Alamofire.request(request).responseJSON { response in
            switch response.result {
            case .success :
                if let JSON = response.result.value as? [String:Any] {
                    if JSON["Error"] != nil {
                        responseHandler(false , JSON["Error"] as? String , nil)
                    }
                    else{
                        responseHandler(true , nil , JSON)
                    }
                }else{
                    responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                }
            case .failure :
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        }
    }
    
    
    class func saveUserInPreference(User user : UserModel) {
        let defaults = UserDefaults.standard
        defaults.set(user.toJSONString(), forKey: "ffflorescenceLoggedUser")
        defaults.synchronize()
    }
    
    class func getUserFromPreference() -> UserModel? {
        let defaults = UserDefaults.standard
        if let userString = defaults.object(forKey: "ffflorescenceLoggedUser") as? String{
            if userString.count == 0 {
                return nil
            }
            else{
                return UserModel.deserialize(from: userString)
            }
        }
        else{
            return nil
        }
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    class func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        //return sourceImage
    }
    
    
    class func captureThumbnail(withVideoURL videoURL:URL,secs:Int,preferredTimeScale scale:Int,completionHandler:((UIImage?) ->Void)?) -> Void{
        //let seconds : Int64 = 10
        // let preferredTimeScale : Int32 = 1
        
        DispatchQueue.global().async {
            
            do
            {
                let asset = AVURLAsset(url: videoURL)
                
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at:CMTimeMake(value: Int64(secs), timescale: Int32(scale)),actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                completionHandler?(thumbnail)
            }
            catch let error as NSError
            {
                print("Error generating thumbnail: \(error)")
                completionHandler?(nil)
            }
        }
    }
    
    
    class func convertStringToDictionary(text: String) -> [String:Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.jpegData(compressionQuality: JPEGQuality.medium.rawValue) else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = index(of: object) else {return}
        remove(at: index)
    }
    
}
