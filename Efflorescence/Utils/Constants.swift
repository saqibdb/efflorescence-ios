//
//  Constants.swift
//
//
//  Created by  on 4/14/17.
//  Copyright © 2017 CCT. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
var auth_token = ""
let app_Name = "Efflorescence"

struct AdsType {
    
    static let Survey = 2
    static let Media = 1
    static let Web = 3
    static let HTML = 5
    static let Coupon = 6
}

struct QuestionType {
    
    static let Text = 1
    static let MultiChoice = 2
    static let Other = 3
}

struct Constant
{
    
    static let countries = ["USA" , "Mexico"]
    
    

    
    static let placeHolderColor = UIColor.init(hex: "595757")
    static let kFontNameMedium = "Roboto-Medium"
    static let kMemberInfo = "MemberInfo"
    static let noInternetMessage = "No network connectivity, try again later."
    
    static let Cookies = "Cookies"
    static let kIsUserLoogedIn = "IsUserLoogedIn"
    static let kFleetName = "FleetName"
    static let kUserFleetID = "kUserFleetID"
    static let kChannelID = "kChannelID"
    
    static let kLanguageName = "kLanguageName"
    static let kLanguageCode = "kLanguageCode"
    static let kLanguageIcon = "kLanguageIcon"
    
    static let navBarColor: UIColor = UIColor.init(hex: "303030")
    
    static let GETLANGUAGEVALUES = "FleetAdmin/GetAppLanguage"
    static let GETAVAILABLELANGUAGE = "FleetAdmin/GetAvailableLanguages"
    

    //static let baseUrl = "http://139.59.216.164:8001"

    static let baseUrl = "https://api-kidsapp.cf"
    //static let baseUrl = "https://9thbitsolutions.com/"

    //static let baseUrl = "https://kids-app-backend.herokuapp.com"
    //static let baseUrl = "http://157.230.40.181:8001"
    
    static let login = "/log_in/"
    static let setPassword = "/set_password/"
    static let setPin = "/set_pin/"
    static let loginWithPin = "/log_in_with_pin/"
    static let getAllTimelinePosts = "/get_all_timeline_posts/"
    static let getAllTimelinePostsFiltered = "/get_all_timeline_posts_filtered/"
    
    static let addTeacher = "/add_teacher_with_class/"
    static let editTeacher = "/edit_teacher/"

    
    
    static let allSchools = "/get_all_schools/"
    static let allBranches = "/get_all_branches/"
    static let allSchoolBranches = "/get_all_school_branches/"

    static let addUser = "/add_user/"
    static let addSubject = "/add_subject/"
    static let editSubject = "/edit_subject/"

    
    
    static let allClasses = "/get_all_classes/"
    static let allTeachers = "/get_all_teachers/"
    static let allParents = "/get_all_parents/" 
    static let removeTeacher = "/remove_teacher/"
    static let removeParent = "/remove_parent/"
    static let removeSchool = "/remove_school/"
    static let removeBranches = "/remove_branch/"
    static let removeClass = "/remove_class/"
    static let addSchool = "/add_school/"
    static let addBranch = "/add_branch/"
    static let editScool = "/edit_school/"
    static let editBranch = "/edit_branch/"
    static let allStudents = "/get_all_student/"
    static let allSubjects = "/get_all_subjects/"
    static let addClassWithSubjectIds = "/add_class_with_subjects_ids/"
    static let editClass = "/edit_class/"
    static let getDetailsOfPost = "/get_details_of_post/"
    static let addCommentToPost = "/add_comment_to_post/"

    static let getTeacherClasses = "/get_all_teacher_classes/"
    static let getStudentOfClass = "/get_all_student_of_class/"
    static let addPost = "/add_post/"

    //static let addPostWithMedia = "/add_post_with_media/"
    static let addPostWithMedia = "/add_post_with_media_and_subjects/"

    static let getChildOfParent = "/get_children_of_parent/"
    static let removePostFromTimeline = "/remove_post_from_timeline/"
    static let updatePostWithMedia = "/edit_post/"
    static let removeCommentFromTimeline = "/remove_comment_from_post/"

    static let addRemoveLikeForPost = "/add_remove_like_for_post/"

    
    static let getSubjects = "/get_all_subjects/"

    static let getAdmins = "/get_all_admins/"
    static let removeSubject = "/remove_subject/"

    
    static let registerDeviceApn = "/register_device_apn/"
    static let unRegisterDeviceApn = "/un_register_device_apn/"
    static let enableDisableNotifications = "/enable_disable_Notifications/"

    
    static let addParentWithChild = "/add_parent_with_students/"

    
}

