//
//  CustomTextField.swift
//  Vixinity App
//
//  Created by Umer Naseer on 11/8/18.
//  Copyright © 2018 Umesh. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField: UITextField
{
    var rowNumber: Int!
    var sectionNumber: Int!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.tintColor = UIColor.init(white: 1.0, alpha: 1.0)
        
        self.SetCustomPlaceHolder(text: placeholder!)
        
    }
    func SetCustomPlaceHolder(text: String)
    {
        self.attributedPlaceholder = NSAttributedString(string: text, attributes:[NSAttributedString.Key.foregroundColor: Constant.placeHolderColor])
    }
}
