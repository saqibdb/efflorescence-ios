//
//  StringExtension.swift
//  Vixinity App
//
//  Created by Umer Naseer on 11/7/18.
//  Copyright © 2018 Umesh. All rights reserved.
//

import Foundation
extension String {
    public func isImageType() -> Bool {
        // image formats which you want to check
        let imageFormats = ["jpg", "png", "gif", "jpeg"]
        
        if URL(string: self) != nil  {
            
            let extensi = (self as NSString).pathExtension.lowercased()
            
            return imageFormats.contains(extensi)
        }
        return false
    }
    func localized() -> String {
        
        var langStr1 = Locale.current.languageCode ?? ""
        
        if langStr1.count == 0
        {
            langStr1 = "en"
        }
        let keyStr = self
        
        return keyStr

        
    }
}
