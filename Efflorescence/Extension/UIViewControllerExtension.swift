//
//  UIViewControllerExtension.swift
//  Vixinity App
//
//  Created by Umer Naseer on 11/1/18.
//  Copyright © 2018 Umesh. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension UIViewController {
    
    
    func alertMessageOk(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
