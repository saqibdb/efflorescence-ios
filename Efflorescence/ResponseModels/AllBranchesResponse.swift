/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct AllBranchesResponse : Mappable {
	var branches : [Branches]?
	var total : Int?
	var nextPage : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		branches <- map["Branches"]
		total <- map["Total"]
		nextPage <- map["NextPage"]
        branches <- map["branches"]

	}

}
struct Branches : Mappable {
    var id : Int?
    var branchName : String?
    var branchAddress : String?
    var creationDate : String?
    var updationDate : String?
    var schoolId : Int?
    var school : School?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        branchName <- map["branchName"]
        branchAddress <- map["branchAddress"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
        schoolId <- map["schoolId"]
        school <- map["school"]
    }
    
}
struct School : Mappable {
    var id : Int?
    var schoolName : String?
    var schoolAddress : String?
    var creationDate : String?
    var updationDate : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        schoolName <- map["schoolName"]
        schoolAddress <- map["schoolAddress"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
    }
    
}
