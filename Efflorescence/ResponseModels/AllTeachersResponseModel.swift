/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct AllTeachersResponseModel : Mappable {
	var teachers : [TeachersData]?
	var total : Int?
	var nextPage : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		teachers <- map["Teachers"]
		total <- map["Total"]
		nextPage <- map["NextPage"]
	}

}
struct BranchData : Mappable {
    var id : Int?
    var branchName : String?
    var branchAddress : String?
    var creationDate : String?
    var updationDate : String?
    var schoolId : Int?
    var school : SchoolData?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        branchName <- map["branchName"]
        branchAddress <- map["branchAddress"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
        schoolId <- map["schoolId"]
        school <- map["school"]
    }
    
}
struct ClassesData : Mappable {
    var id : Int?
    var className : String?
    var creationDate : String?
    var updationDate : String?
    var branchId : Int?
    var isClassTeacher : Bool?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        className <- map["className"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
        branchId <- map["branchId"]
        isClassTeacher <- map["isClassTeacher"]
    }
    
}
struct SchoolData : Mappable {
    var id : Int?
    var schoolName : String?
    var schoolAddress : String?
    var creationDate : String?
    var updationDate : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        schoolName <- map["schoolName"]
        schoolAddress <- map["schoolAddress"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
    }
    
}
struct TeachersData : Mappable {
    var id : Int?
    var userName : String?
    var fullName : String?

    
    var email : String?
    var age : String?
    var phone : String?
    var gender : String?
    var isNotification : Bool?
    var profilePictureURL : String?
    var deviceToken : String?
    var type : String?
    var studentIdentification : String?
    var creationDate : String?
    var updationDate : String?
    var lastLoginDate : String?
    var branchId : Int?
    var branch : BranchData?
    var classes : [ClassesData]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        userName <- map["userName"]
        fullName <- map["fullName"]

        
        
        email <- map["email"]
        age <- map["age"]
        phone <- map["phone"]
        gender <- map["gender"]
        isNotification <- map["isNotification"]
        profilePictureURL <- map["profilePictureURL"]
        deviceToken <- map["deviceToken"]
        type <- map["type"]
        studentIdentification <- map["studentIdentification"]
        creationDate <- map["creationDate"]
        updationDate <- map["updationDate"]
        lastLoginDate <- map["lastLoginDate"]
        branchId <- map["branchId"]
        branch <- map["branch"]
        classes <- map["classes"]
    }
    
}
