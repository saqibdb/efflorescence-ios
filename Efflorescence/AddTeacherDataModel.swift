/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

class AddTeacherDataModel : Mappable {
    required init?(map: Map) {
        
    }
    init() {}
    
	var auth_token : String?
	var user_name : String?
	var email : String?
	var password : String?
	var age : String?
	var phone : String?
	var gender : String?
	var is_notification : Bool?
    var profile_picture_data: String?
	var assigned_classes : [Assigned_classes] = []

	var branch_id : Int?
    var school_id : Int?

    
    
    var full_name : String?

    var type : String?
    var device_token : String?
    var student_identification : String?



    func mapping(map: Map) {

		auth_token <- map["auth_token"]
		user_name <- map["user_name"]
		email <- map["email"]
		password <- map["password"]
		age <- map["age"]
		phone <- map["phone"]
		gender <- map["gender"]
		is_notification <- map["is_notification"]
		assigned_classes <- map["assigned_classes"]
		profile_picture_data <- map["profile_picture_data"]
		branch_id <- map["branch_id"]
        school_id <- map["school_id"]

        
        full_name <- map["full_name"]
        type <- map["type"]
        device_token <- map["device_token"]
        student_identification <- map["student_identification"]

        
	}

}
class Assigned_classes : Mappable {
    var is_class_teacher : Bool?
    var class_id : Int?
    
    required init?(map: Map) {
        
    }
    init() {}
    func mapping(map: Map) {
        
        is_class_teacher <- map["is_class_teacher"]
        class_id <- map["class_id"]
    }
    
}

